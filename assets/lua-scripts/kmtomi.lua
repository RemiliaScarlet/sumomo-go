local sumomo = require("sumomo")

function sumomoDescription()
   return "Converts Kilometers to Miles."
end

function sumomoPerms()
   return "none"
end

function sumomoCommand(server, channel, author, args)
   local numArgs = sumomo.TableLen(args)

   if numArgs == 2 then
      if args[2] == "--help" or args[2] == "-h" then
         return "Usage: **!kmtomi** _<number>_\n"
      end
   end

   km = tonumber(args[2])
   if km == nil then
      return "I can't understand that number ┐(￣ヘ￣;)┌"
   end

   miles = km / 1.609344

   ret = string.format("%.2f° kilometers = %.2f° miles", km, miles)

   if numArgs > 2 then
      ret = ret .. "\n\nbtw... I can only look at one number at a time (^_~)"
   end

   return ret
end
