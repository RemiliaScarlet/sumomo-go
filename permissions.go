// Sumomo-Go
// Copyright(C) 2018-2019 Alexa Jones-Gonzales
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/.>
package main

import (
	"strings"

	"scarletdevilmansion.tech/go/p36lib-go"
)

//
// Permission Checking
//
// Permissions are per-command, and the permission priorities goes
// like this:
//
// * Main admin, allowed on specific channels or not at all
// * Main admin, allowed
// * Specific user/role is denied
// * All other admins, allowed on specific channels or not at all
// * All other admins, allowed
// * Denied on specific channels
// * Allowed on specific channels or not at all
// * Specific user/role is allowed
// * Explicitly allowed on all channels
//
// The permission system goes through these, top to bottom.  As soon
// as one of these is true, the permission is granted.  If the system
// reaches the bottom and nothing is true, the result is automatic
// denial.
//
// Built-in admin commands are exempt from this system.
//

type permissionMap map[string][]string
type permissionList []string

type permsHandler struct {
	// These map command names to a list of user and role IDs
	cmdsAllowed         permissionMap
	cmdsDenied          permissionMap
	cmdsNotOnChannels   permissionMap
	cmdsOnChannels      permissionMap
	mainAdminOnChannels permissionMap
	adminOnChannels     permissionMap

	// A slice of command names
	cmdsAllChannels permissionList
}

func newPermsHandler() *permsHandler {
	return &permsHandler{
		make(permissionMap, 0),
		make(permissionMap, 0),
		make(permissionMap, 0),
		make(permissionMap, 0),
		make(permissionMap, 0),
		make(permissionMap, 0),
		make(permissionList, 0, 0)}
}

//
// Creates a duplicate shallow copy of a permissionMap.
//
func (perms permissionMap) dupPermissionMap() permissionMap {
	ret := make(permissionMap, 0)

	for k, v := range(perms) {
		newArray := make(permissionList, 0, 0)
		copy(newArray, v)
		ret[k] = newArray
	}

	return ret
}

//
// Resets all the permissions to default.
//
func (perms *permsHandler) clearPermissions() {
	perms.cmdsAllowed = make(permissionMap, 0)
	perms.cmdsDenied = make(permissionMap, 0)
	perms.cmdsNotOnChannels = make(permissionMap, 0)
	perms.cmdsOnChannels = make(permissionMap, 0)
	perms.mainAdminOnChannels = make(permissionMap, 0)
	perms.adminOnChannels = make(permissionMap, 0)
	perms.cmdsAllChannels = make(permissionList, 0, 0)
}

func (perms *permsHandler) populateCommand(cmdObj *command, permCollection []string, errs *ErrorMsgCollection) {
	if len(permCollection) == 0 {
		return
	}

	permName := strings.TrimSpace(permCollection[0])
	permArgs := permCollection[1:]

	switch permName {
	case "":
		errs.pushWarn("Command has an empty permission string: %q\n", cmdObj.name)

	case "allow":
		if len(permArgs) > 0 {
			errs.pushErr("Too many arguments to allow permission on %q", cmdObj.name)
		} else {
			perms.cmdsAllChannels = append(perms.cmdsAllChannels, cmdObj.name)
		}

	case "all-admins":
		if len(permArgs) > 0 {
			errs.pushErr("Too many arguments to all-admins permission on %q", cmdObj.name)
		} else {
			// TODO: move this into permsHandler
			cmdObj.needsAdmin = AdminPerm_AnyAdmin
		}

	case "main-admin/channel":
		if len(permArgs) > 1 {
			errs.pushErr("Too many arguments to main-admin/channel permission on %q", cmdObj.name)
		} else {
			perms.mainAdminOnChannels[cmdObj.name] = append(perms.mainAdminOnChannels[cmdObj.name], permArgs[0])
		}

	case "main-admin":
		if len(permArgs) > 0 {
			errs.pushErr("Too many arguments to main-admin permission on %q", cmdObj.name)
		} else {
			// TODO: move this into permsHandler
			cmdObj.needsAdmin = AdminPerm_MainAdmin
		}

	case "deny-user":
		if len(permArgs) > 1 {
			errs.pushErr("Too many arguments to deny-user permission on %q", cmdObj.name)
		} else {
			perms.cmdsDenied[cmdObj.name] = append(perms.cmdsDenied[cmdObj.name], permArgs[0])
		}

	case "any-admin/channel":
		if len(permArgs) > 1 {
			errs.pushErr("Too many arguments to any-admin/channel permission on %q", cmdObj.name)
		} else {
			perms.adminOnChannels[cmdObj.name] = append(perms.adminOnChannels[cmdObj.name], permArgs[0])
		}

	case "not-on-channel":
		if len(permArgs) > 1 {
			errs.pushErr("Too many arguments to not-on-channel permission on %q", cmdObj.name)
		} else {
			perms.cmdsNotOnChannels[cmdObj.name] = append(perms.cmdsNotOnChannels[cmdObj.name], permArgs[0])
		}

	case "only-on-channel":
		if len(permArgs) > 1 {
			errs.pushErr("Too many arguments to only-on-channel permission on %q", cmdObj.name)
		} else {
			perms.cmdsOnChannels[cmdObj.name] = append(perms.cmdsOnChannels[cmdObj.name], permArgs[0])
		}

	case "only-user":
		if len(permArgs) > 1 {
			errs.pushErr("Too many arguments to only-user permission on %q", cmdObj.name)
		} else {
			perms.cmdsAllowed[cmdObj.name] = append(perms.cmdsAllowed[cmdObj.name], permArgs[0])
		}

	default:
		errs.pushErr("Invalid permission in command %q: %q", cmdObj.name, permName)
	}
}

//
// Populats perms with the raw permission data in the loaded BotConfig
// configuration.  Returns true if this was successful, or false
// otherwise.  If it was unsuccessful, the original permissions are
// restored.
//
func (perms *permsHandler) populate(bot *SumomoInstance, conf *Config) bool {
	var cmdObj *command
	ret := true
	errs := NewErrorMsgCollection()

	errs.UseColor = true
	errs.UseWrapping = true
	errs.Output = p36lib.StderrColored

	origCmdsAllowed := perms.cmdsAllowed.dupPermissionMap()
	origCmdsDenied := perms.cmdsDenied.dupPermissionMap()
	origCmdsNotOnChannels := perms.cmdsNotOnChannels.dupPermissionMap()
	origCmdsOnChannels := perms.cmdsOnChannels.dupPermissionMap()
	origMainAdminOnChannels := perms.mainAdminOnChannels.dupPermissionMap()
	origAdminOnChannels := perms.adminOnChannels.dupPermissionMap()

	origCmdsAllChannels := make(permissionList, 0, 0)
	copy(origCmdsAllChannels, perms.cmdsAllChannels)

	for cmd, permCollection := range(conf.Permissions.Commands) {
		if cmdObj = bot.allCommands.findCommand("!" + cmd); cmdObj == nil {
			errs.pushErr( "Invalid command name in permissions: %q", cmd)
			continue
		}

		perms.populateCommand(cmdObj, permCollection, errs)
	}

	if len(errs.Errors) > 0 {
		errs.printErrs("** Errors in Configuration File's Permission Section **")

		perms.cmdsAllowed = origCmdsAllowed
		perms.cmdsDenied = origCmdsDenied
		perms.cmdsNotOnChannels = origCmdsNotOnChannels
		perms.cmdsOnChannels = origCmdsOnChannels
		perms.mainAdminOnChannels = origMainAdminOnChannels
		perms.adminOnChannels = origAdminOnChannels

		ret = false
	}

	if len(errs.Warnings) > 0 {
		errs.printWarns("Configuration File Permission Warnings")
	}

	return ret
}

//
// Given a command and user, see if the command is one of the keys in
// list.  If it is not, this returns false, false.
//
// If the command is one of the keys in list, this then loops through
// the user IDs for that list.  If the user ID matches the given user,
// this returns true, true.
//
// If the command is not one of the keys in list, yet the user was
// indeed found in the list of IDs for that command, this returns
// false, true.
//
// If the command is one of the keys in the list, and the user was not
// found in the list of IDs, this returns true, false.
//
func (perms *permsHandler) checkUserList(cmd, user string, list permissionMap) (bool, bool) {
	var found bool
	if _, found = list[cmd]; found {
		return false, false
	}

	for _, id := range(list[cmd]) {
		if id == user {
			return true, true
		}
	}

	if found {
		return false, true
	} else {
		return true, false
	}
}

//
// Given a user/role ID, channel ID, and a command name, return true
// if the permissions state that the command is allowed to run.
// Otherwise, this returns false.
//
// Please see the priority of permissions at the top of this source
// file.
//
func (bot *SumomoInstance) commandAllowedP(user, channel, command string) bool {
	if bot.userIsMainAdminP(user) {
		// Main admin, allowed on specific channels or not at all
		if foundCmd, inList := bot.Perms.checkUserList(command, user, bot.Perms.mainAdminOnChannels); foundCmd && inList {
			return true
		} else if foundCmd && !inList {
			// The "or not at all" part
			return false
		} else if !foundCmd && inList {
			panic("Permissions are corrupt, found and OK were both false (main admin, per channel)")
		}

		// Main admin, allowed
		return true
	}

	// Specific user/role is denied
	if foundCmd, inList := bot.Perms.checkUserList(command, user, bot.Perms.cmdsDenied); foundCmd && inList {
		return false
	} else if !foundCmd && inList {
		panic("Permissions are corrupt, found and OK were both false (specific user, deny)")
	}

	if bot.userIsAdminP(user) {
		// All other admins, allowed on specific channels or not at all
		if foundCmd, inList := bot.Perms.checkUserList(command, user, bot.Perms.adminOnChannels); foundCmd && inList {
			return true
		} else if foundCmd && !inList {
			// The "or not at all" part
			return false
		} else if !foundCmd && inList {
			panic("Permissions are corrupt, found and OK were both false (admin, per channel)")
		}

		// All other admins, allowed
		return true
	}

	// Denied on specific channels
	if foundCmd, inList := bot.Perms.checkUserList(command, user, bot.Perms.cmdsNotOnChannels); foundCmd && inList {
		return false
	} else if !foundCmd && inList {
		panic("Permissions are corrupt, found and OK were both false (user, not specific channel)")
	}

	// Allowed on specific channels or not at all
	if foundCmd, inList := bot.Perms.checkUserList(command, user, bot.Perms.cmdsOnChannels); foundCmd && inList {
		return true
	} else if foundCmd && !inList {
		// The "or not at all" part
		return false
	} else if !foundCmd && inList {
		panic("Permissions are corrupt, found and OK were both false (user, specific channel)")
	}

	// Specific user/role is allowed
	if foundCmd, inList := bot.Perms.checkUserList(command, user, bot.Perms.cmdsAllowed); foundCmd && inList {
		return true
	} else if !foundCmd && inList {
		panic("Permissions are corrupt, found and OK were both false (user, allowed)")
	}

	// Explicitly allowed on all channels
	for _, checkName := range(bot.Perms.cmdsAllChannels) {
		if command == checkName {
			return true
		}
	}

	// Denied otherwise
	return false
}
