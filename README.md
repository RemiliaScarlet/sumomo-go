# sumomo-go

Simple bot for Discord, written in Go.  This was originally a bot written in
Common Lisp.  The initial conversion was done in the span of a few hours, so the
code is a bit messy at this point.

PRs are welcome.
