// Sumomo-Go
// Copyright(C) 2018-2019 Alexa Jones-Gonzales
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/.>
package main

//
// Feed System
//

import (
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/mmcdole/gofeed"
)

type AbstractFeed interface {
	Name() string
	Channel() string
	Frequency() int
	ContentLimit() int

	NewItemsReadyP() bool
	Fetch() error
	ConstructMessages() []*discordgo.MessageEmbed
}

/*==============================================================================
 * Feed Controller
 *============================================================================*/

//
// Commands for the FeedController
//
type FeedControlCommand int
const (
	FeedCtrl_Reload FeedControlCommand = iota
	FeedCtrl_Pause
	FeedCtrl_Resume
	FeedCtrl_Shutdown
)

//
// How often (in seconds) the FeedController's main loop fires
//
const FeedUpdateFrequency time.Duration = 3

//
// The FeedController handles the Feed system
//
type FeedController struct {
	Comm chan FeedControlCommand
	Feeds []AbstractFeed
	bot *SumomoInstance
}

//
// Initializes all of the feeds.  This constructs AbstractFeed
// instances from the loaded configuration.
//
func (ctrl *FeedController) InitFeeds() {
	ctrl.reloadConfig()
}

//
// Handles the actual populating of the FeedController.Feeds field.
//
func (ctrl *FeedController) reloadConfig() {
	ctrl.Feeds = make([]AbstractFeed, 0, 0)

	for _, feed := range(ctrl.bot.Conf.Feeds.RssFeeds) {
		if feed.UpdateFrequency != -1 {
			ctrl.Feeds = append(ctrl.Feeds, NewRssFeed(feed))
		}
	}
}

//
// Updates all feeds and sends out their messages if any new items are
// available.
//
func (ctrl *FeedController) RunUpdates() {
	for _, feed := range(ctrl.Feeds) {
		if err := feed.Fetch(); err != nil {
			logError("Could not fetch feed: %v\n", err)
			continue
		}

		if feed.NewItemsReadyP() {
			logInfo("Update feed %q\n", feed.Name())

			for _, msg := range(feed.ConstructMessages()) {
				ctrl.bot.Instance.ChannelMessageSendEmbed(feed.Channel(), msg)
				time.Sleep(242 * time.Millisecond) // Lock the target, bait the line, spread the net, catch the man
			}
		}
	}
}

//
// The main loop for the Feed system
//
func (ctrl *FeedController) FeedLoop() {
	paused := false

	for {
		select {
		case command := <-ctrl.Comm:
			switch command {
			case FeedCtrl_Pause:
				paused = true

			case FeedCtrl_Resume:
				paused = false

			case FeedCtrl_Shutdown:
				logInfo("Requested Feed controller shutdown\n")
				return

			case FeedCtrl_Reload:
				ctrl.reloadConfig()

			default:
				panic("Don't know how to handle FeedControlCommand")
			}

		default:
			if !paused {
				ctrl.RunUpdates()
			}

			time.Sleep(FeedUpdateFrequency * time.Second)
		}
	}
}

/*==============================================================================
 * RSS Feeds
 *============================================================================*/

type RssFeed struct {
	name string
	url string
	channel string
	freq int
	strLimit int

	parser *gofeed.Parser
	lastParsed *gofeed.Feed
	lastUpdate time.Time
}

func NewRssFeed(cfg *ConfigRssFeed) *RssFeed {
	return &RssFeed{
		cfg.Name,
		cfg.Url,
		cfg.Channel,
		cfg.UpdateFrequency,
		cfg.ContentLimit,
		gofeed.NewParser(),
		nil,
		time.Now().Add(0)}
}

func (feed *RssFeed) Name() string {
	return feed.name
}

func (feed *RssFeed) Channel() string {
	return feed.channel
}

func (feed *RssFeed) Frequency() int {
	return feed.freq
}

func (feed *RssFeed) ContentLimit() int {
	return feed.strLimit
}

func (feed *RssFeed) NewItemsReadyP() bool {
	return feed.lastParsed.PublishedParsed.After(feed.lastUpdate) &&
		!feed.lastParsed.PublishedParsed.Equal(feed.lastUpdate)
}

func (feed *RssFeed) Fetch() error {
	var err error
	feed.lastParsed, err = feed.parser.ParseURL(feed.url)
	return err
}

func (feed *RssFeed) ConstructMessages() []*discordgo.MessageEmbed {
	ret := make([]*discordgo.MessageEmbed, 0, 0)

	for _, item := range(feed.lastParsed.Items) {
		if item.PublishedParsed.Before(feed.lastUpdate) || item.PublishedParsed.Equal(feed.lastUpdate) {
			continue
		}

		var cont string
		if len(cont) > feed.ContentLimit() {
			cont = cont[:(feed.ContentLimit() - 3)]
			cont = cont + "..."
		} else {
			cont = item.Content
		}

		embed := &discordgo.MessageEmbed{
			Title: item.Title,
			Description: cont}

		ret = append(ret, embed)

		feed.lastUpdate = feed.lastParsed.PublishedParsed.Add(0)
	}

	return ret
}
