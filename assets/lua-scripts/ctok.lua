local sumomo = require("sumomo")

function sumomoDescription()
   return "Converts a Celsius value to Kelvin."
end

function sumomoPerms()
   return "none"
end

function sumomoCommand(server, channel, author, args)
   local numArgs = sumomo.TableLen(args)

   if numArgs == 2 then
      if args[2] == "--help" or args[2] == "-h" then
         return "Usage: **!ftok** _<number>_\n"
      end
   end

   temp = tonumber(args[2])
   if temp == nil then
      return "I can't understand that number ┐(￣ヘ￣;)┌"
   end

   ktemp = temp + 273.15

   ret = string.format("%.1f° C = %.1f° K", temp, ktemp)

   if numArgs > 2 then
      ret = ret .. "\n\nbtw... I can only look at one number at a time (^_~)"
   end

   return ret
end
