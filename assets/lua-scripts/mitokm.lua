local sumomo = require("sumomo")

function sumomoDescription()
   return "Converts Miles to Kilometers."
end

function sumomoPerms()
   return "none"
end

function sumomoCommand(server, channel, author, args)
   local numArgs = sumomo.TableLen(args)

   if numArgs == 2 then
      if args[2] == "--help" or args[2] == "-h" then
         return "Usage: **!mitokm** _<number>_\n"
      end
   end

   miles = tonumber(args[2])
   if miles == nil then
      return "I can't understand that number ┐(￣ヘ￣;)┌"
   end

   km = miles * 1.609344

   ret = string.format("%.2f° miles = %.2f° kilometers", miles, km)

   if numArgs > 2 then
      ret = ret .. "\n\nbtw... I can only look at one number at a time (^_~)"
   end

   return ret
end
