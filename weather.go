// Sumomo-Go
// Copyright(C) 2018-2019 Alexa Jones-Gonzales
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/.>
package main

//
// !weather command
//

import (
	"errors"
	"fmt"
	"math"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
)

import owm "github.com/briandowns/openweathermap"

/*==============================================================================
 * Types
 *============================================================================*/

type weatherData struct {
	receivedAt int64
	data *owm.CurrentWeatherData
	forecast *owm.ForecastWeatherData
}

type WeatherCacheTable map[string]*weatherData

/*==============================================================================
 * Subsystem
 *============================================================================*/

type WeatherSubsystem struct {
	weatherCache WeatherCacheTable
	bot *SumomoInstance
}

func NewWeatherSubsystem() *WeatherSubsystem {
	return &WeatherSubsystem{
		make(WeatherCacheTable, 0),
		nil,
	}
}

func (sys *WeatherSubsystem) Init(bot *SumomoInstance) error {
	sys.bot = bot
	return nil
}

/*==============================================================================
 * Weather Retrieval
 *============================================================================*/

//
// Given a query string, check the cached weather data to see if we
// already have the weather info.  If we do, check to see if it's too
// old.  If both of these are true, that cached weather data is
// returned.  Otherwise, this retrieves new weather data and returns
// it.
//
// If an error is encountered, this will return (nil, error).
//
func (sys *WeatherSubsystem) getWeatherInfo(query string) (*weatherData, error) {
	var ret *weatherData
	var weather *owm.CurrentWeatherData
	var forecast *owm.ForecastWeatherData
	var err error

	if cached, found := sys.weatherCache[strings.ToLower(query)]; found {
		logDebug("Cached weather data found\n")

		if time.Now().Unix() - cached.receivedAt < 600 { // 10 minutes
			logDebug("Cached weather is still valid, returning it\n")
			return cached, nil
		} else {
			logDebug("Cached weather is too old\n")
		}
	}

	logInfo("Retrieving new weather data\n")
	if weather, err = owm.NewCurrent("C", "EN", sys.bot.Conf.Weather.Key); err != nil {
		return nil, err
	} else if err = weather.CurrentByName(query); err != nil {
		return nil, err
	}

	if sys.bot.Conf.Weather.IncludeForecasts {
		logInfo("Retrieving new forecast data\n")
		if forecast, err = owm.NewForecast("16", "C", "EN", sys.bot.Conf.Weather.Key); err != nil {
			return nil, err
		} else if err = forecast.DailyByName(query, 16); err != nil {
			return nil, err
		}
	}

	ret = &weatherData{time.Now().Unix(), weather, forecast}
	sys.weatherCache[query] = ret

	return ret, nil
}

/*==============================================================================
 * Functions for generating the response strings
 *============================================================================*/

//
// Given some OWM weather data and the number of days (num), this
// generates a string containing the descriptive forecast data.  On
// success, that string and nil are returned.  Otherwise an empty
// string and an error are returned.
//
func (sys *WeatherSubsystem) generateSingleForecastString(data *owm.ForecastWeatherData, num int) (string, error) {
	var ret strings.Builder
	var forecast *owm.Forecast16WeatherData

	if val, ok := data.ForecastWeatherJson.(*owm.Forecast16WeatherData); ok {
		forecast = val
	} else {
		return "", errors.New("Expected a 16 day forecast, but got something different.")
	}

	if forecast.Cnt != 16 {
		return "", errors.New(fmt.Sprintf("Expected to find 16 days of weather, but only got %d.", forecast.Cnt))
	}

	weather := forecast.List[num]

	cLow  := weather.Temp.Min
	cHigh := weather.Temp.Max
	fLow  := cToF(cLow)
	fHigh := cToF(cHigh)

	for idx, wd := range weather.Weather {
		icon, found := WeatherIcons[weather.Weather[0].Icon]

		if !found {
			icon = ":question:"
		}

		ret.WriteString(fmt.Sprintf("%s %s", icon, wd.Description))

		if idx < len(weather.Weather) - 1 {
			ret.WriteString(", ")
		} else {
			ret.WriteString("\n")
		}
	}
	ret.WriteString(fmt.Sprintf("High: %.1f° C / %.1f° F\nLow: %.1f° C / %.1f° C\n", cHigh, fHigh, cLow, fLow))

	return ret.String(), nil
}

//
// Generates the descriptive forecast response for a *weatherData
// instance.  On success, that string and nil are returned.  Otherwise
// an empty string and an error are returned.
//
func (sys *WeatherSubsystem) generateForecastResponse(data *weatherData) (string, error) {
	var ret strings.Builder
	today := time.Now()

	ret.WriteString(fmt.Sprintf("**Forecast as of %s:**\n", today.UTC().Format(time.RFC850)))

	for i := 0; i < sys.bot.Conf.Weather.NumForecastDays; i++ {
		if str, err := sys.generateSingleForecastString(data.forecast, i); err != nil {
			return "", err
		} else {
			if i == 0 {
				ret.WriteString("**Today:**\n")
			} else if i == 1 {
				ret.WriteString("**Tomorrow:**\n")
			} else {
				iDay := time.Weekday(i)
				iDay = (today.Weekday() + iDay) % 6

				ret.WriteString(fmt.Sprintf("**%s:**\n", time.Weekday(iDay)))
			}

			ret.WriteString(str)
			ret.WriteString("\n")
		}
	}

	return ret.String(), nil
}

//
// Given a day number (synodic month), return the description of the
// moon's phase.
//
func (sys *WeatherSubsystem) moonPhaseDayDesc(day int) string {
	if day == 0 || day == 29 {
		return "New Moon"
	} else if day > 0 && day < 7 {
		return "Waxing Crescent"
	} else if day == 7 {
		return "First Quarter"
	} else if day > 7 && day < 15 {
		return "Waxing Gibbous"
	} else if day == 15 {
		return "Full Moon"
	} else if day > 15 && day < 22 {
		return "Waning Gibbous"
	} else if day == 22 {
		return "Last Quarter"
	} else if day > 22 {
		return "Waning Crescent"
	} else {
		return "INVALID DATE FOR MOON PHASE"
	}
}

//
// Given a day number (synodic month), return the emoji that shows the
// moon's phase.
//
func (sys *WeatherSubsystem) moonPhaseDayEmoji(day int) string {
	if day == 0 || day == 29 {
		return ":new_moon:"
	} else if day > 0 && day < 7 {
		return ":waxing_crescent_moon:"
	} else if day == 7 {
		return ":first_quarter_moon:"
	} else if day > 7 && day < 15 {
		return ":waxing_gibbous_moon:"
	} else if day == 15 {
		return ":full_moon:"
	} else if day > 15 && day < 22 {
		return ":waning_gibbous_moon:"
	} else if day == 22 {
		return ":last_quarter_moon:"
	} else if day > 22 {
		return ":waning_crescent_moon:"
	} else {
		return ":question:"
	}
}

//
// Calculates the moon's illumination percentage.
//
func (sys *WeatherSubsystem) calculateMoonIllum() float64 {
	date := time.Now().Add(-48 * time.Hour)
	jd := float64(getJulianDay(&date))
	jc := (jd - 730455.5) / 36525

	// mean elongation of the moon
	moonElong := 297.8501921 + jc * (445267.1114034 + jc * (-0.0018819 + jc * (1.0 / 545868 - jc / 113065000)))

	// suns mean anomaly
	sunAnom := 357.5291092 + jc * (35999.0502909 + jc * (-0.0001536 + jc / 24490000))

	// moons mean anomaly
	moonAnom := 134.9633964 + jc * (477198.8675055 + jc * (0.0087414 + jc * (1.0 / 69699 - jc / 14712000)))

	// Calculate phase angle
	phaseAngle := 180 - moonElong - 6.289 * math.Sin(moonAnom * deg2Rad) +
		2.100 * math.Sin(sunAnom * deg2Rad) -
		1.274 * math.Sin((2 * moonElong - moonAnom) * deg2Rad) -
		0.658 * math.Sin(2 * moonElong * deg2Rad) -
		0.214 * math.Sin(2 * moonAnom * deg2Rad) -
		0.110 * math.Sin(moonElong * deg2Rad)

	// Adjust phase angle
	if phaseAngle < 0 {
		phaseAngle = -phaseAngle
	}

	if phaseAngle >= 360 {
		phaseAngle = phaseAngle - math.Floor(phaseAngle / 360) * 360
	}

	// Calculate illumination
	ret := (1 + math.Cos(phaseAngle * deg2Rad)) / 2;

	//return ret * 100
	return math.Round(100 + (ret * -100))
}

//
// Calculates the moon's phase and returns the number of days into
// this synodic month.
//
// Adapted from http://ben-daglish.net/moon.shtml
// Rest In Peace
//
func (sys *WeatherSubsystem) calculateMoonPhase() int {
	date := time.Now()
	thisJD := float64(getJulianDay(&date))
	year := float64(date.Year())

	K0 := math.Floor((year - 1900) * 12.3685)
	T := (year - 1899.5) / 100
	T2 := T * T
	T3 := T * T * T
	J0 := 2415020 + 29 * K0
	F0 := 0.0001178 * T2 - 0.000000155 * T3 + (0.75933 + 0.53058868 * K0) - (0.000837 * T + 0.000335 * T2)
	M0 := 360 * (getFractionalPart(K0 * 0.08084821133)) + 359.2242 - 0.0000333 * T2 - 0.00000347 * T3
	M1 := 360 * (getFractionalPart(K0 * 0.07171366128)) + 306.0253 + 0.0107306 * T2 + 0.00001236 * T3
	B1 := 360 * (getFractionalPart(K0 * 0.08519585128)) + 21.2964 - (0.0016528 * T2) - (0.00000239 * T3)

	var oldJ float64
	var phase float64
	var jday float64

	for jday < thisJD {
		F := F0 + 1.530588 * phase
		M5 := (M0 + phase * 29.10535608) * deg2Rad
		M6 := (M1 + phase * 385.81691806) * deg2Rad
		B6 := (B1 + phase * 390.67050646) * deg2Rad

		F -= 0.4068 * math.Sin(M6) + (0.1734 - 0.000393 * T) * math.Sin(M5)
		F += 0.0161 * math.Sin(2 * M6) + 0.0104 * math.Sin(2 * B6)
		F -= 0.0074 * math.Sin(M5 - M6) - 0.0051 * math.Sin(M5 + M6)
		F += 0.0021 * math.Sin(2 * M5) + 0.0010 * math.Sin(2 * B6 - M6)
		F += 0.5 / 1440

		oldJ = jday
		jday = J0 + 28 * phase + math.Floor(F)
		phase++
	}

	phaseDay := int(math.Floor(thisJD - oldJ)) % 30
	return phaseDay
}

//
// Generates a complete descriptive weather response.  On success,
// that string and nil are returned.  Otherwise an empty string and an
// error are returned.
func (sys *WeatherSubsystem) generateWeatherResponse(query string, data *weatherData) (string, error) {
	var ret strings.Builder
	weather := data.data
	icon, found := WeatherIcons[weather.Weather[0].Icon]

	if !found {
		icon = ":question:"
	}

	cTemp := weather.Main.Temp
	fTemp := cToF(cTemp)
	kTemp := cToK(cTemp)

	ret.WriteString(fmt.Sprintf("**%s Current conditions for %s**\n%.1f° C / %.1f° F / %.1f° K, ",
		icon, strings.Title(query), cTemp, fTemp, kTemp))

	for idx, wd := range weather.Weather {
		ret.WriteString(fmt.Sprintf("%s", wd.Description))

		if idx < len(weather.Weather) - 1 {
			ret.WriteString(", ")
		} else {
			ret.WriteString("\n")
		}
	}

	ret.WriteString(fmt.Sprintf("Humidity: %d%%\n", weather.Main.Humidity))
	ret.WriteString(fmt.Sprintf("Wind: %.0f m/s, %s\n", weather.Wind.Speed, degreesToCardinal(weather.Wind.Deg)))

	moonDays := sys.calculateMoonPhase()
	moonIllum := sys.calculateMoonIllum()
	ret.WriteString("\n**Astronomical Data:**\n")
	ret.WriteString(fmt.Sprintf("%s %s\n", sys.moonPhaseDayEmoji(moonDays), sys.moonPhaseDayDesc(moonDays)))
	ret.WriteString(fmt.Sprintf("%.0f%% illuminated\n", moonIllum))
	ret.WriteString(fmt.Sprintf("%d days into this synodic month, %d days until New Moon", moonDays, (29 - moonDays)))

	if weather.Snow.ThreeH > 0.0 {
		ret.WriteString(fmt.Sprintf("\nSnowfall, last three hours: %.1f cm\n", (weather.Snow.ThreeH / 10.0)))
	}

	if weather.Rain.ThreeH > 0.0 {
		ret.WriteString(fmt.Sprintf("\nRain, last three hours: %.1f mm\n", weather.Rain.ThreeH))
	}

	if sys.bot.Conf.Weather.IncludeForecasts {
		ret.WriteString("\n")

		if str, err := sys.generateForecastResponse(data); err != nil {
			return "", err
		} else {
			ret.WriteString(str)
		}
	}

	return ret.String(), nil
}

//
// Implements the !weather command
//
func (bot *SumomoInstance) cmdWeather(msg *discordgo.MessageCreate, args []string) {
	if bot.Conf.Weather.Key == "" {
		bot.sendMsg(msg.ChannelID, "Sorry, the !weather command isn't currently available.")
		return
	}

	logDebug("!weather args: %s\n", args)
	bot.Instance.ChannelTyping(msg.ChannelID)

	if len(args) <= 1 {
		bot.sendMsg(msg.ChannelID, `Usage: **!weather** _<location query>_

If you aren't sure what to do for a query, try going to <https://openweathermap.org/> and searching there.  Then use the same query here.`)
		return
	}

	// Easter eggs
	if len(args) == 2 && strings.ToLower(args[1]) == "vagina" {
		bot.sendMsg(msg.ChannelID, "uhh... \"moist\"...")
		return
	} else if len(args) == 2 && strings.ToLower(args[1]) == "penis" {
		bot.sendMsg(msg.ChannelID, "I dunno, it's too \"hard\" to look up that one.")
		return
	} else if len(args) == 2 && strings.ToLower(args[1]) == "gensokyo" {
		bot.sendMsg(msg.ChannelID, "https://www.youtube.com/watch?v=WwrqRuYH2HA")
		return
	} else if len(args) == 2 && strings.ToLower(args[1]) == "boobs" {
		bot.sendMsg(msg.ChannelID, "Oppai :3")
		return
	} else if len(args) == 2 && strings.ToLower(args[1]) == "mars" {
		bot.sendMsg(msg.ChannelID, "Cloudy with a chance of demonic invasion")
		return
	} else if len(args) == 2 && (strings.ToLower(args[1]) == "innsmouth" || strings.ToLower(args[1]) == "r'lyeh") {
		bot.sendMsg(msg.ChannelID, "Yog-Sothoth knows the gate. Yog-Sothoth is the gate. Yog-Sothoth is the key and guardian of the gate. Past, present, future, all are one in Yog-Sothoth. He knows where the Old Ones broke through of old, and where They shall break through again.")
		return
	}

	query := strings.Join(args[1:], " ")

	if weather, err := bot.WeatherSys.getWeatherInfo(query); err != nil {
		bot.sendMsg(msg.ChannelID, "Error getting weather data for %s (｡•́︿•̀｡)", query)
	} else {
		if str, err := bot.WeatherSys.generateWeatherResponse(query, weather); err != nil {
			bot.sendMainAdminDM("Error generating weather: %s\n", err)
			bot.sendMsg(msg.ChannelID, "Whoops!  Something went wrong.  I notified the main admin about it.\n")
		} else {
			// Send the message raw
			bot.Instance.ChannelMessageSend(msg.ChannelID, str)
		}
	}
}
