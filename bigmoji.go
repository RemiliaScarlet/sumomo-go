// Sumomo-Go
// Copyright(C) 2018-2019 Alexa Jones-Gonzales
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/.>
package main

//
// !bigmoji command
//

import (
	"bytes"
	"errors"
	"fmt"
	"image"
	"image/draw"
	"image/gif"
	"image/png"
	"net/http"
	"net/url"
	"strings"
	"time"
	"unicode/utf8"

	"github.com/bwmarrin/discordgo"
	"github.com/disintegration/imaging"
)

type BigmojiSubsystem struct {
	cachedEmoji *emojiCache
	bot *SumomoInstance
}

func NewBigmojiSubsystem() *BigmojiSubsystem {
	return &BigmojiSubsystem{
		nil,
		nil,
	}
}

func (sys *BigmojiSubsystem) Init(bot *SumomoInstance) error {
	sys.bot = bot
	sys.cachedEmoji = &emojiCache{
		sys,
		bot.Conf.Bigmoji.CacheLimit,
		make(map[string]*processedEmoji, 0),
		make([]string, 0, 0),
		false,
	}

	sys.bot.Instance.State.TrackEmojis = true

	go sys.cachedEmoji.cacheCleanerProc()

	return nil
}

/*============================================================================*/

//
// Holds the data representing a single emoji after it has been
// processed (resized) and is thus ready for use by !bigmoji.
//
type processedEmoji struct {
	name string
	filename string
	madeAt time.Time
	data []byte
}

//
// Resized emoji cache.  This is in two parts, the actual hash table,
// and a slice that acts as a stack.  The oldest cached images are
// removed first if the cache gets too large.
//
type emojiCache struct {
	sub *BigmojiSubsystem
	limit int
	cache map[string]*processedEmoji
	queue []string
	cleaningCache bool
}

//
// Returns the *processedEmoji,bool where the given name matches the
// name of a *processedEmoji in the emoji cache.  The bool will be
// true if one was successfully found, or false otherwise.
//
// Note that the cache is first pruned before the search is done.
//
func (cache *emojiCache) getCache(name string) (*processedEmoji, bool) {
	for cache.cleaningCache {
		time.Sleep(1 * time.Microsecond)
	}

	cache.pruneOld()
	ret, found := cache.cache[name]
	return ret, found
}

//
// Adds an entry to the emoji cache with the given name.  The name,
// filename, and data parameters are stored in a new *processedEmoji
// instance.
//
// After adding the new emoji, the cache will be pruned.
//
// If an emoji with the same name already exists in the cache, a
// logWarning() will be issued.
//
func (cache *emojiCache) cacheBytes(name string, filename string, data []byte) {
	if _, found := cache.cache[name]; found {
		logWarning("Duplicate emoji cached: %q\n", name)
	}

	cache.cache[name] = &processedEmoji{
		name,
		filename,
		time.Now().Add(0),
		data}
	cache.queue = append(cache.queue, name)

	cache.pruneOld()
}

//
// Prunes the emoji cache of old *processedEmoji instances.  The
// number of cached instances depends on the configuration.
//
func (cache *emojiCache) pruneOld() {
	cache.cleaningCache = true
	if len(cache.cache) > cache.limit {
		logDebug("Too many emoji cached, removing oldest\n")
		toRemove := cache.queue[0]
		cache.queue = cache.queue[1:]
		delete(cache.cache, toRemove)
	} else {
		logDebug("Not enough emoji to do a prune, ignoring\n")
	}
	cache.cleaningCache = false
}

func (cache *emojiCache) cacheCleanerProc() {
	logDebug("Starting BigmojiSubsystem.cacheCleanerProc()\n")

	for {
		time.Sleep(time.Duration(cache.sub.bot.Conf.Bigmoji.CacheCleanTime) * time.Second)

		cache.cleaningCache = true
		if len(cache.cache) > 0 {
			logDebug("Popping oldest emoji off of the bigmoji cache\n")
			toRemove := cache.queue[0]
			cache.queue = cache.queue[1:]
			delete(cache.cache, toRemove)
		}
		cache.cleaningCache = false
	}
}

/*============================================================================*/

//
// Returns true if name is a valid symbolic name for an image filter,
// or false otherwise.
//
func validImagingFilter(name string) bool {
	switch strings.ToLower(name) {
	case "nearest":
		return true
	case "bspline":
		return true
	case "bartlett":
		return true
	case "blackman":
		return true
	case "box":
		return true
	case "catmullrom":
		return true
	case "cosine":
		return true
	case "gaussian":
		return true
	case "hamming":
		return true
	case "hann":
		return true
	case "hermite":
		return true
	case "lanczos":
		return true
	case "linear":
		return true
	case "mitchellnetravali":
		return true
	case "welch":
		return true
	default:
		return false
	}
}

//
// Returns the imaging.ResampleFilter that matches the current
// configuration's descriptive name for the filter.  If the
// configuration somehow has an invalid descriptive name, a logError()
// will be issued, then imaging.NearestNeighbor will be returned.
//
func (sys *BigmojiSubsystem) getImagingFilter() imaging.ResampleFilter {
	switch sys.bot.Conf.Images.Filter {
	case "nearest":
		return imaging.NearestNeighbor
	case "bspline":
		return imaging.BSpline
	case "bartlett":
		return imaging.Bartlett
	case "blackman":
		return imaging.Blackman
	case "box":
		return imaging.Box
	case "catmullrom":
		return imaging.CatmullRom
	case "cosine":
		return imaging.Cosine
	case "gaussian":
		return imaging.Gaussian
	case "hamming":
		return imaging.Hamming
	case "hann":
		return imaging.Hann
	case "hermite":
		return imaging.Hermite
	case "lanczos":
		return imaging.Lanczos
	case "linear":
		return imaging.Linear
	case "mitchellnetravali":
		return imaging.MitchellNetravali
	case "welch":
 		return imaging.Welch
	default:
		logError("Invalid imaging filter in bot config: %s", sys.bot.Conf.Images.Filter)
		return imaging.NearestNeighbor
	}
}

/*============================================================================*/

//
// Resizes the frames of an animated gif.
//
func (sys *BigmojiSubsystem) resizeAnimatedEmoji(img *gif.GIF) *gif.GIF {
	for i := 0; i < len(img.Image); i++ {
		// Create the resized frame and get its size
		rframe := imaging.Resize(img.Image[i], sys.bot.Conf.Bigmoji.Resize, 0, sys.getImagingFilter())
		size := rframe.Bounds()

		// Change the base image's size
		img.Config.Height = size.Max.Y
		img.Config.Width = size.Max.X

		// Paletize
		frame := image.NewPaletted(size, img.Image[i].Palette)
		draw.FloydSteinberg.Draw(frame, size, rframe, image.ZP)

		// Set the new frame
		img.Image[i] = frame
	}

	return img
}

/*============================================================================*/

//
// Attempts to get an animated emoji.  On success, the gif and
// isAnimatedErr are returned.  If it appears not to be an animated
// emoji, nil and notAnimatedErr are returned.  All other errors
// result in nil and an error being returned.
//
func (sys *BigmojiSubsystem) getAnimatedEmoji(emoji string) (*gif.GIF, error) {
	logDebug("Attempting to get animated emoji\n")

	resp, err := http.Get(fmt.Sprintf("https://cdn.discordapp.com/emojis/%s.gif", url.QueryEscape(emoji)))
	defer resp.Body.Close()

	if err != nil {
		logDebug("HTTP error getting server emoji\n")
		return nil, err
	}

	if resp.StatusCode == http.StatusUnsupportedMediaType {
		logDebug("Not an animated emoji\n")
		return nil, notAnimatedErr
	}

	if resp.StatusCode != http.StatusNotFound {
		logDebug("Emoji found, attempting to decode\n")

		origImg, err := gif.DecodeAll(resp.Body)
		if err != nil {
			logDebug("Emoji decoding error\n")
			return nil, err
		}

		logDebug("Returning decoded emoji GIF\n")
		return origImg, isAnimatedErr
	} else {
		return nil, nil
	}
}

//
// Gets an emoji that's specific to a server.  This does not
// necessarily mean the server that Sumomo is running on.
//
func (sys *BigmojiSubsystem) getServerEmoji(emoji string) (image.Image, error) {
	logDebug("Getting server-specific with ID %s\n", emoji)

	resp, err := http.Get(fmt.Sprintf("https://cdn.discordapp.com/emojis/%s.png", url.QueryEscape(emoji)))
	defer resp.Body.Close()

	if err != nil {
		logDebug("HTTP error getting server emoji\n")
		return nil, err
	}

	if resp.StatusCode != http.StatusNotFound {
		logDebug("Emoji found, attempting to decode\n")

		origImg, _, err := image.Decode(resp.Body)
		if err != nil {
			logDebug("Emoji decoding error\n")
			return nil, err
		}

		logDebug("Returning decoded emoji PNG\n")
		return origImg, nil
	} else {
		return nil, nil
	}
}

//
// Downloads a copy of a stock emoji from twemoji, then returns it.
//
func (sys *BigmojiSubsystem) getStockEmoji(emoji string) (image.Image, error) {
		// A few emoji are longer than one rune, so decode the entire
	// string one rune at a time.
	var emojiHex string
	var c int

	for {
		r, length := utf8.DecodeRuneInString(emoji[c:])

		if emojiHex == "" {
			emojiHex = fmt.Sprintf("%x", r)
		} else {
			emojiHex = fmt.Sprintf("%s-%x", emojiHex, r)
		}

		c += length
		if c >= len(emoji) {
			break
		}
	}

	logDebug("Getting twemoji for %q (hex: %s)\n", emoji, emojiHex)

	resp, err := http.Get(fmt.Sprintf("https://twemoji.maxcdn.com/2/72x72/%s.png", emojiHex))
	defer resp.Body.Close()

	if err != nil {
		logDebug("HTTP error getting emoji\n")
		return nil, err
	}

	if resp.StatusCode != http.StatusNotFound {
		logDebug("Emoji found, attempting to decode\n")

		origImg, _, err := image.Decode(resp.Body)
		if err != nil {
			logDebug("Emoji decoding error\n")
			return nil, err
		}

		logDebug("Returning decoded emoji PNG\n")
		return origImg, nil
	} else {
		// Emoji was not found at all
		return nil, nil
	}
}

//
// Attempts to get an emoji matching the given name.  On success, a
// byte slice containing the resized version of the emoji and the
// filename for it is returned, as well as a nil error.
//
// If any situation is encountered where it cannot return a properly
// processed emoji, then an empty byte array, an empty string, and an
// error is returned.
//
func (sys *BigmojiSubsystem) getEmoji(emoji string) ([]byte, string, error) {
	var gifImg *gif.GIF
	var origImg image.Image
	var isAnimated bool
	var filename string
	var err error
	var ret []byte

	logDebug("Attempting to download emoji\n")

	// Get the emoji
	if utf8.RuneCountInString(emoji) <= 2 {
		// Single character probably means a stock emoji
		origImg, err = sys.getStockEmoji(emoji)
	} else {
		// Multiple characters probably means we should first try to get an animated emoji
		if gifImg, err = sys.getAnimatedEmoji(emoji); err != notAnimatedErr && err != isAnimatedErr {
			return ret, "", errors.New("Could not get an animated emoji")
		} else if err == isAnimatedErr {
			// It is indeed animated
			logDebug("Emoji appears to be animated\n")
			isAnimated = true
		} else if err == notAnimatedErr {
			// Not animated, try to get a server emoji
			logDebug("Emoji appears not to be animated, getting normal server emoji\n")
			origImg, err = sys.getServerEmoji(emoji)
			isAnimated = false

			if err != nil {
				// Well crap
				return ret, "", errors.New("Could not get server emoji")
			}
		} else {
			return ret, "", errors.New("Could not get any emoji")
		}
	}

	logDebug("Resizing emoji\n")
	var newImgIo bytes.Buffer

	if isAnimated {
		gifImg = sys.resizeAnimatedEmoji(gifImg)
		filename = "bigified-by-sumomo.gif"

		gif.EncodeAll(&newImgIo, gifImg)
	} else {
		if origImg == nil {
			return ret, "", errors.New("The emoji does not seem to exist")
		}

		origImg = imaging.Resize(origImg, sys.bot.Conf.Bigmoji.Resize, 0, sys.getImagingFilter())
		filename = "bigified-by-sumomo.png"

		enc := &png.Encoder{ CompressionLevel: png.BestCompression }
		enc.Encode(&newImgIo, origImg)

	}

	return newImgIo.Bytes(), filename, nil
}

/*============================================================================*/

//
// Implements the !bigmoji command
//
func (bot *SumomoInstance) cmdBigmoji(msg *discordgo.MessageCreate, args []string) {
	logInfo("!bigmoji called: %q\n", args)

	if bot.maybeSendHelp(msg, args, `Usage: **!bigmoji** _<emoji with the colons around it>_`) {
		return
	}

	// Get the ID (emojiStr) and name (emojiName)
	emojiStr := args[1]
	logDebug("Raw emoji string: %q\n", emojiStr)
	emojiStr = strings.Trim(emojiStr, "<>")

	// Check to see that we can correctly cut the ID out
	if bits := strings.Split(emojiStr, ":"); len(bits) != 3 && len(bits) != 1 {
		logDebug("Not enough bits split out, only got %d: %q\n", len(bits), bits)
		bot.sendMsg(msg.ChannelID, "Whoops, couldn't see that emoji.  .･ﾟﾟ･(／ω＼)･ﾟﾟ･.")
		return
	} else {
		if len(bits) == 3 {
			emojiStr = bits[2]
		} else {
			emojiStr = bits[0]
		}

		logDebug("Emoji ID number (or symbol): %q\n", emojiStr)
	}

	// Construct the return message
	bot.Instance.ChannelTyping(msg.ChannelID)

	var err error
	var filename string
	var data []byte
	img, found := bot.BigmojiSys.cachedEmoji.getCache(emojiStr)

	if found {
		data = img.data
		filename = img.filename
	} else {
		data, filename, err = bot.BigmojiSys.getEmoji(emojiStr)

		if err != nil {
			logError("Error getting emoji: %s\n", err)
			bot.sendMsg(msg.ChannelID, "Whoops, couldn't see that emoji.  .･ﾟﾟ･(／ω＼)･ﾟﾟ･.")
			return
		}

		bot.BigmojiSys.cachedEmoji.cacheBytes(emojiStr, filename, data)
	}

	newImgReader := bytes.NewReader(data)
	embed := &discordgo.MessageSend{
		Embed: &discordgo.MessageEmbed{
			Image: &discordgo.MessageEmbedImage{
				URL: "attachment://" + filename}},

		Files: []*discordgo.File{
			&discordgo.File{
				Name:   filename,
				Reader: newImgReader}}}

	// ~~Unleash annoyance onto the world~~
	bot.Instance.ChannelMessageSendComplex(msg.ChannelID, embed)
}
