// Sumomo-Go
// Copyright(C) 2018-2019 Alexa Jones-Gonzales
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/.>
package main

//
// Lua Scripting API
//

import (
	"math/rand"
	"time"

	"github.com/yuin/gopher-lua"
)

// Lua functions in the "sumomo" module
var sumomoLuaFuncs = map[string]lua.LGFunction{
	"LogInfo": luaLogInfo,
	"LogWarn": luaLogWarn,
	"LogError": luaLogError,
	"LogDebug": luaLogDebug,
	"TableLen": luaTableLen,
	"RandomInt": luaRandomInt,
}

func initLuaSumomoModule(state *lua.LState) int {
	mod := state.SetFuncs(state.NewTable(), sumomoLuaFuncs)
	state.Push(mod)

	return 1
}

func luaLogInfo(state *lua.LState) int {
	numArgs := state.GetTop()

	if numArgs == 0 {
		state.RaiseError("Not enough arguments to LogInfo")
		return 0

	} else if numArgs > 1 {
		state.RaiseError("Too many arguments to LogInfo")
		return 0
	}

	logInfo("Lua LogInfo: %v", state.ToString(1))

	return 0
}

func luaLogWarn(state *lua.LState) int {
	numArgs := state.GetTop()

	if numArgs == 0 {
		state.RaiseError("Not enough arguments to LogWarn")
		return 0

	} else if numArgs > 1 {
		state.RaiseError("Too many arguments to LogWarn")
		return 0
	}

	logWarning("Lua LogWarn: %v", state.ToString(1))

	return 0
}

func luaLogError(state *lua.LState) int {
	numArgs := state.GetTop()

	if numArgs == 0 {
		state.RaiseError("Not enough arguments to LogError")
		return 0

	} else if numArgs > 1 {
		state.RaiseError("Too many arguments to LogError")
		return 0
	}

	logError("Lua LogError: %v", state.ToString(1))

	return 0
}

func luaLogDebug(state *lua.LState) int {
	numArgs := state.GetTop()

	if numArgs == 0 {
		state.RaiseError("Not enough arguments to LogDebug")
		return 0
	} else if numArgs > 1 {
		state.RaiseError("Too many arguments to LogDebug")
		return 0
	}

	logDebug("Lua LogDebug: %v", state.ToString(1))

	return 0
}

func luaTableLen(state *lua.LState) int {
	numArgs := state.GetTop()

	if numArgs == 0 {
		state.RaiseError("TableLen requires a table")
		return 0
	} else if numArgs > 1 {
		state.RaiseError("TableLen requires a single argument")
		return 0
	}

	tbl := state.ToTable(1)
	if tbl == nil {
		state.RaiseError("TableLen requires a table")
		return 0
	}

	state.Push(lua.LNumber(tbl.Len()))
	return 1
}

// Implements the sumomo.RandomInt Lua module function.  All numbers
// returned fit into 64-bits, and any bounds must fit within 64-bits.
//
// sumomo.RandomInt() - Returns a random integer
// sumomo.RandomInt(upperBound) - Returns a random integer between 0 and upperBound
// sumomo.RandomInt(lower, upper) - Returns a random integer between lower and upper
func luaRandomInt(state *lua.LState) int {
	numArgs := state.GetTop()
	rndSrc := rand.NewSource(time.Now().UnixNano())
	rnd := rand.New(rndSrc)

	if numArgs == 0 {
		ret := rnd.Int63()
		state.Push(lua.LNumber(ret))

	} else if numArgs == 1 {
		upper := state.CheckInt64(1) + 1
		ret := rnd.Int63n(upper)
		state.Push(lua.LNumber(ret))

	} else if numArgs == 2 {
		lower := state.CheckInt64(1)
		upper := state.CheckInt64(2) + 1

		if lower < 0 {
			state.RaiseError("RandomInt lower bound cannot be negative")
			return 0
		}

		if lower >= upper {
			state.RaiseError("RandomInt's lower bound must be less than its upper bound")
			return 0
		}

		ret := rnd.Int63n(upper - lower) + lower
		state.Push(lua.LNumber(ret))
	}

	return 1
}
