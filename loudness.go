// Sumomo-Go
// Copyright(C) 2018-2019 Alexa Jones-Gonzales
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/.>
package main

//
// Loudness subsystem
//

import (
	"fmt"
	"math/rand"
	"regexp"
	"strings"
	"time"
	"unicode"

	"github.com/bwmarrin/discordgo"
)

type LoudnessSubsystem struct {
	bot *SumomoInstance
	lastAnnounce time.Time
}

func NewLoudnessSubsystem() *LoudnessSubsystem {
	now := time.Now().Add(0)
	return &LoudnessSubsystem{
		nil,
		now,
	}
}

func (sys *LoudnessSubsystem) Init(bot *SumomoInstance) error {
	sys.bot = bot
	sys.lastAnnounce = time.Now().Add(time.Duration(sys.bot.Conf.Loudness.LoudnessWaitTime) * time.Second * -1)
	return nil
}

//
// Checks to see if a user posted with their cruise control set to
// cool.
//
func (sys *LoudnessSubsystem) HandleLoudPosters(msg *discordgo.MessageCreate) {
	if sys.bot.Conf.Loudness.LoudnessMinLen == -1 {
		return
	}

	logDebug("Checking for loud poster\n")
	// The Regular expression is to remove user IDs
	r := regexp.MustCompile("\\<@\\!?[0-9]+\\>")
	str := r.ReplaceAllString(msg.Content, "")

	// This one is to remove emoji
	r = regexp.MustCompile(`:[a-zA-Z0-9]+:`)
	str = r.ReplaceAllString(str, "")

	// See if the message meets the minimum length requirement
	if sys.bot.Conf.Loudness.LoudnessMinLen > 0 && len(str) < sys.bot.Conf.Loudness.LoudnessMinLen {
		logDebug("Loud poster control: string length (%d) does not meet minimum requirement (%d)\n",
			len(str), sys.bot.Conf.Loudness.LoudnessMinLen)
		return
	}

	percent := sys.percentUppercase(str) * 100
	logDebug("Loud poster control: percent of last message: %f (threshold: %f)\n", percent,
		float64(sys.bot.Conf.Loudness.LoudnessThreshold))

	if percent >= float64(sys.bot.Conf.Loudness.LoudnessThreshold) {
		now := time.Now()
		if now.Sub(sys.lastAnnounce).Seconds() < float64(sys.bot.Conf.Loudness.LoudnessWaitTime) {
			logDebug("Last loud poster message was done too recently, not sending another\n")
			return
		} else {
			sys.lastAnnounce = now
		}

		logDebug("CAPSLOCK IS CRUISE CONTROL FOR COOL (for %s)\n", msg.Author.ID)
		num := rand.Intn(len(CapsLockMessages))
		ret := fmt.Sprintf("HEY <@%s>, CAPS LOCK MIGHT BE ON\n%s\n", msg.Author.ID, CapsLockMessages[num])
		sys.bot.sendMsg(msg.ChannelID, ret)
	}
}

//
// Given a string, return the percentage of characters that are
// uppercase in it as a float64.
//
func (sys *LoudnessSubsystem) percentUppercase(str string) float64 {
	compStr := str
	// Remove non-loud words
	for _, word := range(sys.bot.Conf.Loudness.NotLoudWords) {
		compStr = strings.Replace(compStr, strings.ToUpper(word), "", -1)
	}

	// Determine the number of lower case letters (and how many runes
	// we should ignore)
	numUpper := 0.0
	numIgnore := 0.0
	strLen := float64(len(compStr))
	for _, c := range(compStr) {
		if unicode.IsLetter(c) {
			if unicode.IsUpper(c) {
				numUpper++
			}
		} else {
			numIgnore++
		}
	}

	// Prevent divide by zero
	if strLen - numIgnore == 0 {
		return 0.0
	}

	// Calculate and return
	ret :=  numUpper / (strLen - numIgnore)
	logDebug("Calculated an uppercase percent: %v (number of uppercase characters was %v, out of %v, ignored %v)\n",
		ret, numUpper, float64(strLen), numIgnore)
	return ret
}
