local sumomo = require("sumomo")

function sumomoDescription()
   return "Ask a magic 8-ball a question."
end

function sumomoPerms()
   return "none"
end


EightBallAnswers = {
   "It is certain.",
   "It is decidedly so.",
   "Without a doubt.",
   "Yes - definitely.",
   "You may rely on it.",
   "As I see it, yes.",
   "Most likely.",
   "Outlook good.",
   "Yes.",
   "Signs point to yes.",

   "Reply is hazy, try again.",
   "Ask again later.",
   "Better not tell you now.",
   "Cannot predict right now.",
   "Concentrate and ask again.",

   "Don't count on it.",
   "My reply is \"no\".",
   "My sources say no.",
   "The outlook is not so good.",
   "Very doubtful.",
}


function sumomoCommand(server, channel, author, args)
   local numArgs = sumomo.TableLen(args)

   if numArgs == 2 then
      if args[2] == "--help" or args[2] == "-h" then
         return "Usage: **!8ball** _<your question>_\n"
      end
   end

   local question = ""
   for i, v in pairs(args) do
      if i ~= 1 then
         question = question .. v
         if i < numArgs then
            question = question .. " "
         end
      end
   end

   if question == "" then
      return "Usage: **!8ball** _<your question>_\n"
   end

   return string.format("Ok magic 8-ball... _%s_\n\n:8ball: **%s**\n",
                        question,
                        EightBallAnswers[sumomo.RandomInt(1, 20)])
end
