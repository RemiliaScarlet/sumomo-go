// Sumomo-Go
// Copyright(C) 2018-2019 Alexa Jones-Gonzales
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/.>
package main

//
// Configuration File Handling
//

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"

	"github.com/hjson/hjson-go"
)

type ConfigGlobal struct {
	Token             string
	LogLevel          string
	BotLogLevel       string
	Timezone          string
	LuaDirectory      string

	NowPlaying        []string
	NowPlayingTime    int
}

type ConfigServerLog struct {
	EventLogTypes []string // Only used for parsing the config file
	LogChannel string
	EventLogOptions ServerLogEventType `json:"-"` // Not in config file
}

type ConfigPermissions struct {
	Admin               string
	OtherAdmins         []string
	AdminControlChannel string
	Commands            map[string][]string
}

type ConfigLoudness struct {
	NotLoudWords      []string
	LoudnessMinLen    int
	LoudnessThreshold int
	LoudnessWaitTime  int
}

type ConfigImages struct {
	Filter string
}

type ConfigReminders struct {
	Database  string
	CheckTime int
}

type ConfigWeather struct {
	Key              string
	BothUnits        bool
	IncludeForecasts bool
	NumForecastDays  int
}

type ConfigBigmoji struct {
	CacheLimit     int
	CacheCleanTime int
	Resize         int
}

type ConfigRssFeed struct {
	Name string
	Url string
	Channel string
	UpdateFrequency int
	ContentLimit int
}

type ConfigFeeds struct {
	RssFeeds []*ConfigRssFeed
}

type Config struct {
	Global      ConfigGlobal
	Permissions ConfigPermissions
	ServerLog   ConfigServerLog
	Loudness    ConfigLoudness
	Images      ConfigImages
	Reminders   ConfigReminders
	Weather     ConfigWeather
	Feeds       ConfigFeeds
	Bigmoji     ConfigBigmoji
}

//
// Parses a configuration file stored in the given filename.  On
// success, a *config and nil are returned.  Otherwise nil and an
// error are returned.
//
func parseConfigFile(filename string) (*Config, error) {
	file, err := os.Open(filename)
	defer file.Close()

	if err != nil {
		return nil, err
	}

	return parseConfigStream(file)
}

func parseConfigStream(in io.Reader) (*Config, error) {
	var hjsonData map[string]interface{}
	var ret Config

	rawStr, err := ioutil.ReadAll(in)
	if err != nil {
		return nil, err
	}

	// hjson -> JSON
	if err := hjson.Unmarshal(rawStr, &hjsonData); err != nil {
		return nil, err
	}

	// Unmarshal the JSON
	if jsonDat, err := json.Marshal(hjsonData); err != nil {
		return nil, err
	} else if err = json.Unmarshal(jsonDat, &ret); err != nil {
		return nil, err
	}

	return &ret, nil
}

//
// Checks that the configuration is valid.  The return values are as follow:
//   * First []string: A list of error messages
//   * Second []string: A list of warning messages
//   * Third []string: A list of informational messages
//   * Bool: True if the configuration is valid, or false otherwise
//
func (conf *Config) checkConfiguration() (*ErrorMsgCollection, bool) {
	errs := NewErrorMsgCollection()
	ret:= true

	conf.Permissions.Admin = strings.TrimSpace(conf.Permissions.Admin)
	if conf.Permissions.Admin == "" {
		errs.pushErr("Config Error: No admin user set")
		ret = false
	}

	conf.Permissions.AdminControlChannel = strings.TrimSpace(conf.Permissions.AdminControlChannel)
	if conf.Permissions.AdminControlChannel == "" {
		errs.pushErr("Config Error: No admin control channel set")
		ret = false
	}

	conf.Global.Token = strings.TrimSpace(conf.Global.Token)
	if conf.Global.Token == "" {
		errs.pushErr("Config Error: No Discord token set")
		ret = false
	}

	conf.Global.LogLevel = strings.ToLower(strings.TrimSpace(conf.Global.LogLevel))
	if conf.Global.LogLevel != "error" && conf.Global.LogLevel != "warning" &&
		conf.Global.LogLevel != "info" && conf.Global.LogLevel != "debug" {

		if conf.Global.LogLevel == "" {
			errs.pushWarn("Config Warning: No log level set, defaulting to \"error\"")
			conf.Global.LogLevel = "error"
		} else {
			errs.pushErr("Config Error: Invalid log level: %q", conf.Global.LogLevel)
			ret = false
		}
	}

	conf.Global.Timezone = strings.TrimSpace(conf.Global.Timezone)
	if conf.Global.Timezone == "" {
		errs.pushWarn("Config warning: No Timezone set, defaulting to 'unknown'")
		conf.Global.Timezone = "unknown"
	}

	conf.Global.BotLogLevel = strings.ToLower(strings.TrimSpace(conf.Global.BotLogLevel))
	if conf.Global.BotLogLevel != "error" && conf.Global.BotLogLevel != "warning" &&
		conf.Global.BotLogLevel != "info" && conf.Global.BotLogLevel != "debug" {

		if conf.Global.BotLogLevel == "" {
			errs.pushWarn("Config Warning: No bot log level set, defaulting to \"error\"")
			conf.Global.BotLogLevel = "error"
		} else {
			errs.pushErr("Config Error: Invalid bot log level: %q", conf.Global.BotLogLevel)
			ret = false
		}
	}

	conf.Global.LuaDirectory = strings.TrimSpace(conf.Global.LuaDirectory)
	fmt.Printf("LUA DIR: %q\n", conf.Global.LuaDirectory)
	if conf.Global.LuaDirectory == "" {
		errs.pushWarn("No Lua directory set, Lua scripts disabled")

	} else if info, err := os.Stat(conf.Global.LuaDirectory); os.IsNotExist(err) {
		errs.pushWarn("Lua directory does not exist, Lua scripts disabled: %q", conf.Global.LuaDirectory)
		conf.Global.LuaDirectory = ""

	} else if !info.IsDir() {
		errs.pushWarn("Lua directory is not an actual directory, Lua scripts disabled")
		conf.Global.LuaDirectory = ""
	}

	conf.ServerLog.LogChannel = strings.ToLower(strings.TrimSpace(conf.ServerLog.LogChannel))
	if conf.ServerLog.LogChannel == "" {
		errs.pushWarn("No server logging channel set, events will not be logged")
	} else {
		errs.pushNote("Server event logging channel: %v", conf.ServerLog.LogChannel)
	}

	conf.ServerLog.EventLogOptions = 0
	for idx, evtType := range(conf.ServerLog.EventLogTypes) {
		trimmed := strings.ToLower(strings.TrimSpace(evtType))
		conf.ServerLog.EventLogTypes[idx] = trimmed

		switch trimmed {
		case "user-join":
			conf.ServerLog.EventLogOptions |= Event_UserJoin
		case "user-leave":
			conf.ServerLog.EventLogOptions |= Event_UserLeave
		//case "message-delete":
		//	conf.ServerLog.EventLogOptions |= Event_MsgDelete
		//case "message-edit":
		//	conf.ServerLog.EventLogOptions |= Event_MsgEdit
		case "pings":
			conf.ServerLog.EventLogOptions |= Event_Ping
		case "user-update":
			conf.ServerLog.EventLogOptions |= Event_UserUpdate

		default:
			errs.pushErr("Invalid server event logging type: %v", trimmed)
		}
	}

	if conf.Loudness.LoudnessMinLen < -1 {
		errs.pushErr("Config Error: Invalid loudness mininum length: %v", conf.Loudness.LoudnessMinLen)
		ret = false
	}

	if conf.Loudness.LoudnessThreshold < 1 || conf.Loudness.LoudnessThreshold > 100 {
		errs.pushErr("Config Error: Invalid loudness threshold: %v", conf.Loudness.LoudnessThreshold)
		ret = false
	}

	if conf.Loudness.LoudnessWaitTime < 0 {
		errs.pushErr("Config Error: Invalid loudness wait time: %v", conf.Loudness.LoudnessWaitTime)
		ret = false
	}

	if conf.Global.NowPlayingTime < 0 {
		errs.pushErr("Config Error: Invalid now playing time: %v", conf.Global.NowPlayingTime)
		ret = false
	}

	conf.Images.Filter = strings.ToLower(strings.TrimSpace(conf.Images.Filter))
	if !validImagingFilter(conf.Images.Filter) {
		errs.pushErr("Config Error: Invalid image filter: %q", conf.Images.Filter)
		ret = false
	}

	conf.Reminders.Database = strings.TrimSpace(conf.Reminders.Database)
	if conf.Reminders.Database == "" {
		errs.pushWarn("Config Warning: No reminders database set, !reminders will be disabled")
	} else {
		if info, err := os.Stat(conf.Reminders.Database); err != nil {
			errs.pushNote("Config Note: Could not stat reminders database, !reminders database will be created")
		} else if info.IsDir() {
			errs.pushWarn("Config Warning: The reminders database is not a file, !reminders will be disabled")
		}
	}

	if conf.Reminders.CheckTime < 1 {
		errs.pushErr("Config Error: Invalid reminders check time: %d", conf.Reminders.CheckTime)
		ret = false
	}

	conf.Weather.Key = strings.TrimSpace(conf.Weather.Key)
	if conf.Weather.Key == "" {
		errs.pushWarn("Config Warning: No weather API key set, !weather will be disabled")
	}

	if conf.Weather.NumForecastDays < 1 || conf.Weather.NumForecastDays > 16 {
		errs.pushErr("Config Error: Invalid number of forecast days: %d", conf.Weather.NumForecastDays)
		ret = false
	}

	if conf.Bigmoji.CacheLimit < 1 {
		errs.pushErr("Config Error: Invalid bigmoji cache limit: %d", conf.Bigmoji.CacheLimit)
		ret = false
	}

	if conf.Bigmoji.CacheCleanTime < 30 {
		errs.pushErr("Config Error: Invalid bigmoji cache cleaning time: %d", conf.Bigmoji.CacheCleanTime)
		ret = false
	}

	if conf.Bigmoji.Resize < 1 {
		errs.pushErr("Config Error: Invalid bigmoji resize: %d", conf.Bigmoji.Resize)
		ret = false
	}

	return errs, ret
}

func (conf *Config) generateConfigReport() string {
	var ret strings.Builder

	ret.WriteString("\n=============\n")
	ret.WriteString("Configuration\n")
	ret.WriteString("=============\n")

	ret.WriteString(fmt.Sprintf("Admin user: %s\n", conf.Permissions.Admin))

	ret.WriteString("Other admins: ")
	if len(conf.Permissions.OtherAdmins) > 0 {
		for idx, user := range(conf.Permissions.OtherAdmins) {
			ret.WriteString(user)

			if idx < len(conf.Permissions.OtherAdmins) - 1 {
				ret.WriteString(", ")
			}
		}
		ret.WriteString("\n")
	} else {
		ret.WriteString("(none)\n")
	}

	ret.WriteString(fmt.Sprintf("Admin Control Channel ID: %s\n", conf.Permissions.AdminControlChannel))
	ret.WriteString(fmt.Sprintf("Token: %s\n", strings.Repeat("*", len(conf.Global.Token))))
	ret.WriteString(fmt.Sprintf("Log Level: %s\n", conf.Global.LogLevel))
	ret.WriteString(fmt.Sprintf("Bot Log Level: %s\n", conf.Global.BotLogLevel))
	ret.WriteString(fmt.Sprintf("Timezone: %s\n", conf.Global.Timezone))
	ret.WriteString(fmt.Sprintf("Lua Directory: %s\n", conf.Global.LuaDirectory))

	ret.WriteString(fmt.Sprintf("Server Logging Channel: %s\n", conf.ServerLog.LogChannel))
	ret.WriteString("Server logging events: ")
	if conf.ServerLog.LogChannel == "" {
		ret.WriteString("(none, no logging channel set)\n\n")
	} else if len(conf.ServerLog.EventLogTypes) == 0 {
		ret.WriteString("(none)\n\n")
	} else {
		for idx, evt := range(conf.ServerLog.EventLogTypes) {
			ret.WriteString(evt)

			if idx < len(conf.ServerLog.EventLogTypes) - 1 {
				ret.WriteString(", ")
			}
		}
		ret.WriteString("\n\n")
	}

	ret.WriteString(fmt.Sprintf("Loudness Minimum Length: %v\n", conf.Loudness.LoudnessMinLen))
	ret.WriteString(fmt.Sprintf("Loudness Threshold: %v%%\n", conf.Loudness.LoudnessThreshold))
	ret.WriteString(fmt.Sprintf("Loudness Wait Time: %v\n", conf.Loudness.LoudnessWaitTime))

	ret.WriteString("Words That Aren't Loud: ")
	if len(conf.Loudness.NotLoudWords) > 0 {
		for idx, word := range(conf.Loudness.NotLoudWords) {
			ret.WriteString(word)

			if idx < len(conf.Loudness.NotLoudWords) - 1 {
				ret.WriteString(", ")
			}
		}
		ret.WriteString("\n\n")
	} else {
		ret.WriteString("(none)\n\n")
	}

	ret.WriteString(fmt.Sprintf("Now Playing Time: %d\n", conf.Global.NowPlayingTime))
	ret.WriteString("Now Playing Messages:\n")
	if len(conf.Global.NowPlaying) > 0 {
		for _, str := range(conf.Global.NowPlaying) {
			ret.WriteString(fmt.Sprintf("  * %q\n", str))
		}
		ret.WriteString("\n")
	} else {
		ret.WriteString("(none)\n")
	}

	ret.WriteString(fmt.Sprintf("Image Filter: %s\n\n", conf.Images.Filter))

	ret.WriteString(fmt.Sprintf("Reminder Database: %s\n", conf.Reminders.Database))
	ret.WriteString(fmt.Sprintf("Reminder Check Time: %d\n\n", conf.Reminders.CheckTime))

	ret.WriteString(fmt.Sprintf("Weather Key: %s\n", strings.Repeat("*", len(conf.Weather.Key))))
	ret.WriteString(fmt.Sprintf("Return both units: %v\n", conf.Weather.BothUnits))
	ret.WriteString(fmt.Sprintf("Include forecasts: %v\n", conf.Weather.IncludeForecasts))
	ret.WriteString(fmt.Sprintf("Num forecast days: %d\n\n", conf.Weather.NumForecastDays))

	ret.WriteString(fmt.Sprintf("Bigmoji Cache Limit: %d\n", conf.Bigmoji.CacheLimit))
	ret.WriteString(fmt.Sprintf("Bigmoji Cache Clean Time: %d\n", conf.Bigmoji.CacheCleanTime))
	ret.WriteString(fmt.Sprintf("Bigmoji Resize: %d\n", conf.Bigmoji.Resize))

	ret.WriteString("=================\n")
	ret.WriteString("End Configuration\n")
	ret.WriteString("=================\n\n")

	return ret.String()
}

//
// Prints a textual report for a configuration to standard output
//
func (conf *Config) reportConfig() {
	fmt.Printf(conf.generateConfigReport())
}

//
// Checks that the loaded configuration is valid, and prints any
// messages to a string.  That string is then returned, as well as a
// bool indicating if the configurationis valid (true if it is, false
// otherwise).
//
// This is similar to doCmdLineConfigCheck(), except that it builds a
// string without colors, and works on a specific *Config instance.
//
func (conf *Config) checkConfigWithReport() (string, bool) {
	errs, ok := conf.checkConfiguration()
	ret := bytes.NewBufferString("")
	valid := true

	errs.UseColor = false
	errs.UseWrapping = false
	errs.Output = ret

	ret.WriteString("**== Configuration Check ==**\n")
	if !ok {
		valid = false

		if len(errs.Errors) == 0 {
			ret.WriteString("UH OH: Length of errors should NOT be 0\n\n")
		} else {
			errs.printErrs("Config Errors")
			ret.WriteRune('\n')
		}
	} else if ok && len(errs.Errors) != 0 {
		ret.WriteString("UH OH: Length of errors should be 0")
	}

	errs.printWarns("Config Warnings")
	ret.WriteRune('\n')

	errs.printNotes("Config Notes")
	ret.WriteRune('\n')

	if valid {
		ret.WriteString("**== Configuration looks good ==**\n")
	} else {
		ret.WriteString("**== Configuration errors ==**\n")
	}

	return ret.String(), valid
}
