// Sumomo-Go
// Copyright(C) 2018-2019 Alexa Jones-Gonzales
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/.>
package main

import (
	"testing"
)

func TestExistingSubsystems(t *testing.T) {
	var sys SumomoSubsystem

	if sys = NewReminderSubystem(); sys == nil {
		t.Error("Reminder subsystem returned nil\n")
	}

	if sys = NewBigmojiSubsystem(); sys == nil {
		t.Error("Bigmoji subsystem returned nil\n")
	}

	if sys = NewDictSubsystem(); sys == nil {
		t.Error("Dict subsystem returned nil\n")
	}

	if sys = NewWeatherSubsystem(); sys == nil {
		t.Error("Weather subsystem returned nil\n")
	}

	if sys = NewLoudnessSubsystem(); sys == nil {
		t.Error("Loudness subsystem returned nil\n")
	}
}
