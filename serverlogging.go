// Sumomo-Go
// Copyright(C) 2018-2019 Alexa Jones-Gonzales
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/.>
package main

//
// Server Logging Subsystem
//

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
)

/*==============================================================================
 * Types
 *============================================================================*/

type ServerLogEventType int
const (
	Event_UserJoin   ServerLogEventType = 1
	Event_UserLeave  ServerLogEventType = 2
	//Event_MsgDelete  ServerLogEventType = 4
	//Event_MsgEdit    ServerLogEventType = 8
	Event_Ping       ServerLogEventType = 16
	Event_UserUpdate ServerLogEventType = 32
)

type ServerLogEvent struct {
	evtType ServerLogEventType
	msg string
}

/*==============================================================================
 * Subsystem
 *============================================================================*/

type ServerLogSubsystem struct {
	bot *SumomoInstance
	msgChan chan *ServerLogEvent
}

func NewServerLogSubsystem() *ServerLogSubsystem {
	return &ServerLogSubsystem{
		nil,
		make(chan *ServerLogEvent),
	}
}

func (sys *ServerLogSubsystem) Init(bot *SumomoInstance) error {
	sys.bot = bot
	go sys.run()

	return nil
}

/*==============================================================================
 * Backend
 *============================================================================*/

func (sys *ServerLogSubsystem) logServerEvent(evt *ServerLogEvent) {
	title := ""
	color := 0xff0000

	switch evt.evtType {
	case Event_UserJoin:
		title = ":heavy_plus_sign: **User Joined:**\n"
		color = 0x5dffdf

	case Event_UserLeave:
		title = ":heavy_minus_sign: **User Left:**\n"
		color = 0x234fff

	case Event_UserUpdate:
		title = ":busts_in_silhouette: **User Updated:**\n"
		color = 0x16ff39

	case Event_Ping:
		title = ":telephone_receiver: **User Pinged Someone:**"
		color = 0x9900ff
	}

	if sys.bot.Conf.ServerLog.LogChannel == "" {
		logWarning("Attempted to log server event, but no logging channel set.\n")
		logInfo("Unlogged server event: %s - %s\n", title, evt.msg)
		return
	}

	logInfo(fmt.Sprintf("%s\n%s\n", title, evt.msg))

	embed := &discordgo.MessageEmbed{
		Color: color,
		Title: title,
		Description: evt.msg}

	sys.bot.Instance.ChannelMessageSendEmbed(sys.bot.Conf.ServerLog.LogChannel, embed)
}

func (sys *ServerLogSubsystem) run() {
	logInfo("Starting server logging subsystem\n")

	for {
		evt := <-sys.msgChan
		sys.logServerEvent(evt)
	}
}

/*==============================================================================
 * Frontend
 *============================================================================*/

func (sys *ServerLogSubsystem) logMentions(guildID string, user *discordgo.User, channelID string, mentions []*discordgo.User) {
	if sys.bot.Conf.ServerLog.EventLogOptions & Event_Ping == 0 {
		return
	}

	go func(){
		var msg strings.Builder
		member, err := sys.bot.Instance.GuildMember(guildID, user.ID)
		authorName := ""

		if err == nil {
			authorName = member.Nick
		} else {
			authorName = fmt.Sprintf("(UNKNOWN, error occured: %q)", err)
		}

		msg.WriteString(fmt.Sprintf("**Channel (ID):** <#%s> (%s)\n", channelID, channelID))
		msg.WriteString("**User doing the pinging (username, ID):**\n")
		msg.WriteString(fmt.Sprintf("%s (%s#%s, %s)\n\n", authorName, user.Username, user.Discriminator, user.ID))

		msg.WriteString("**Users pinged:**\n")
		for _, ping := range(mentions) {
			member, err = sys.bot.Instance.GuildMember(guildID, ping.ID)

			if err == nil {
				authorName = member.Nick
			} else {
				authorName = fmt.Sprintf("(UNKNOWN, error occured: %q)", err)
			}

			msg.WriteString(fmt.Sprintf("%s (%s#%s)\n", authorName, ping.Username, ping.Discriminator))
		}

		sys.msgChan <- &ServerLogEvent{Event_Ping, strings.TrimSpace(msg.String())}
	}()
}

func (sys *ServerLogSubsystem) onMemberJoin(bot *discordgo.Session, event *discordgo.GuildMemberAdd) {
	var msg strings.Builder

	msg.WriteString(fmt.Sprintf("**Username:** %s\n", event.User.Username))
	msg.WriteString(fmt.Sprintf("**Nickname:** %s\n", event.Nick))
	msg.WriteString(fmt.Sprintf("**ID:** %s\n", event.User.ID))

	// Determine account age.  If this gets used more, please move to
	// a new function in utils.go
	if id, err := strconv.ParseInt(event.User.ID, 10, 64); err != nil {
		msg.WriteString("Could not determine account age, bad ID\n")
	} else {
		// Extract ID from snowflake
		id = (id >> 22) + 1420070400000
		id /= 1000
		created := time.Unix(id, 0)
		span := time.Now().Sub(created)

		fyears := span.Hours() / 24 / 365
		fmonths := (fyears - float64(int64(fyears))) * 12
		fdays := (fmonths - float64(int64(fmonths))) * 30.416666666666668 // Average days per month
		msg.WriteString(fmt.Sprintf("**Account created:** %s\n(about %d years, %d months, %d days ago)",
			created.String(), int64(fyears), int64(fmonths), int64(fdays)))
	}

	sys.msgChan <- &ServerLogEvent{Event_UserJoin, msg.String()}
}

func (sys *ServerLogSubsystem) onMemberLeave(bot *discordgo.Session, event *discordgo.GuildMemberRemove) {
	var msg strings.Builder

	msg.WriteString(fmt.Sprintf("**Username:** %s\n", event.User.Username))
	msg.WriteString(fmt.Sprintf("**Nickname:** %v\n", event.Nick))
	msg.WriteString(fmt.Sprintf("**ID:** %s", event.User.ID))

	sys.msgChan <- &ServerLogEvent{Event_UserLeave, msg.String()}
}

func (sys *ServerLogSubsystem) onUserUpdate(bot *discordgo.Session, event *discordgo.GuildMemberUpdate) {
	// var msg strings.Builder

	// msg.WriteString(fmt.Sprintf("**Username (ID):** %s#%s (%s)\n",
	// 	event.User.Username, event.User.Discriminator, event.User.ID))
	// msg.WriteString(fmt.Sprintf("**New Nick: %s**\n", event.Nick))

	// sys.msgChan <- &ServerLogEvent{Event_UserUpdate, msg.String()}
}
