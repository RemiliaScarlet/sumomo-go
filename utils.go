// Sumomo-Go
// Copyright(C) 2018-2019 Alexa Jones-Gonzales
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/.>
package main

//
// Utility Functions
//

import (
	"fmt"
	"io"
	"math"
	"os"
	"strings"
	"time"

	"scarletdevilmansion.tech/go/p36lib-go"
	"scarletdevilmansion.tech/go/p36log-go/v2"

	"github.com/Djarvur/parsewords"
)

/*----------------------------------------------------------------------------*/

//
// The ErrorStrList type simply makes it easier to collect a list of
// error messages and print them out.
//

type ErrorMsgCollection struct {
	Errors []string
	Warnings []string
	Notes []string

	UseColor bool
	UseWrapping bool
	Output io.Writer
}

func NewErrorMsgCollection() *ErrorMsgCollection {
	return &ErrorMsgCollection{
		make([]string, 0, 0),
		make([]string, 0, 0),
		make([]string, 0, 0),
		true,
		true,
		nil}
}

func (obj *ErrorMsgCollection) pushErr(msg string, args ...interface{}) {
	obj.Errors = append(obj.Errors, fmt.Sprintf(msg, args...))
}

func (obj *ErrorMsgCollection) pushWarn(msg string, args ...interface{}) {
	obj.Warnings = append(obj.Warnings, fmt.Sprintf(msg, args...))
}

func (obj *ErrorMsgCollection) pushNote(msg string, args ...interface{}) {
	obj.Notes = append(obj.Notes, fmt.Sprintf(msg, args...))
}

func (obj *ErrorMsgCollection) printErrs(header string) {
	out := obj.Output
	if out == nil {
		out = p36lib.StderrColored
	}

	if obj.UseColor {
		p36lib.StreamFormatColored(out, p36lib.FormatRedBold, "%s: ", header)
	} else {
		fmt.Fprintf(out, "%s: ", header)
	}

	if len(obj.Errors) > 0 {
		fmt.Fprintf(out, "\n")
		for _, err := range(obj.Errors) {
			if obj.UseWrapping {
				p36lib.FormatWrapSpecial(out, 80, 5, 0, "  => %s\n", err)
			} else {
				fmt.Fprintf(out, "  => %s\n", err)
			}
		}
	} else {
		fmt.Fprintf(out, "(none)\n")
	}
}

func (obj *ErrorMsgCollection) printWarns(header string) {
	out := obj.Output
	if out == nil {
		out = p36lib.StderrColored
	}

	if obj.UseColor {
		p36lib.StreamFormatColored(out, p36lib.FormatYellowBold, "%s: ", header)
	} else {
		fmt.Fprintf(out, "%s: ", header)
	}

	if len(obj.Warnings) > 0 {
		fmt.Fprintf(out, "\n")
		for _, err := range(obj.Warnings) {
			if obj.UseWrapping {
				p36lib.FormatWrapSpecial(out, 80, 5, 0, "  => %s\n", err)
			} else {
				fmt.Fprintf(out, "  => %s\n", err)
			}
		}
	} else {
		fmt.Fprintf(out, "(none)\n")
	}
}

func (obj *ErrorMsgCollection) printNotes(header string) {
	out := obj.Output
	if out == nil {
		out = p36lib.StdoutColored
	}

	if obj.UseColor {
		p36lib.StreamFormatColored(out, p36lib.FormatBlueBold, "%s: ", header)
	} else {
		fmt.Fprintf(out, "%s: ", header)
	}

	if len(obj.Notes) > 0 {
		fmt.Fprintf(out, "\n")
		for _, err := range(obj.Notes) {
			if obj.UseWrapping {
				p36lib.FormatWrapSpecial(out, 80, 5, 0, "  => %s\n", err)
			} else {
				fmt.Fprintf(out, "  => %s\n", err)
			}
		}
	} else {
		fmt.Fprintf(out, "(none)\n")
	}
}

/*----------------------------------------------------------------------------*/

//
// Given a string, return a slice of strings where the original string
// is separated by whitespace.
//
func splitAsArgs(str string) []string {
	ret, err := parsewords.Quotewords(" ", parsewords.KeepNothing, str)
	if err != nil {
		logDebug("Bad string passed to splitAsArgs: %q  (Error was %s)\n", str, err)
		return strings.Fields(str)
	}

	return ret
}

//
// Converts a degree to a cardinal direction, returning that direction
// as an abbreviation in a string.
//
func degreesToCardinal(deg float64) string {
	if deg >= 0.0 && deg <= 22.0 {
		return "NNE"
	} else if deg > 22.0 && deg <= 45.0 {
		return "NE"
	} else if deg > 45.0 && deg <= 67.0 {
		return "ENE"
	} else if deg > 67.0 && deg <= 90.0 {
		return "E"
	} else if deg > 90.0 && deg <= 112.0 {
		return "ESE"
	} else if deg > 112.0 && deg <= 135.0 {
		return "SE"
	} else if deg > 135.0 && deg <= 157.0 {
		return "SSE"
	} else if deg > 157.0 && deg <= 180.0 {
		return "S"
	} else if deg > 180.0 && deg <= 202.0 {
		return "SSW"
	} else if deg > 202.0 && deg <= 225.0 {
		return "SW"
	} else if deg > 225.0 && deg <= 247.0 {
		return "WSW"
	} else if deg > 247.0 && deg <= 270.0 {
		return "W"
	} else if deg > 270.0 && deg <= 292.0 {
		return "WNW"
	} else if deg > 292.0 && deg <= 315.0 {
		return "NW"
	} else if deg > 315.0 && deg <= 337.0 {
		return "NNW"
	} else {
		return "N"
	}
}

//
// Fahrenheit to Celsius conversion
//
func fToC(temp float64) float64 {
	return ((temp - 32) * 5) / 9
}


//
// Celsius to Fahrenheit conversion
//
func cToF(temp float64) float64 {
	return ((temp * 9) / 5) + 32
}

//
// Celsius to Kelvin conversion
//
func cToK(temp float64) float64 {
	return temp + 273.15
}

//
// Prints a formatted message to standard error, then exits the
// program with the code 255.
//
func die(fmtString string, fmtArgs ...interface{}) {
	fmt.Fprintf(os.Stderr, fmtString, fmtArgs...)
	os.Exit(255)
}

//
// Logs a formatted error message to standard error.
//
func logError(msg string, fmtArgs ...interface{}) {
	p36log.SError(msg, fmtArgs...)
}

//
// Logs a formatted warning message to standard error if the log
// level is at least "warn"
//
func logWarning(msg string, fmtArgs ...interface{}) {
	p36log.SWarn(msg, fmtArgs...)
}

//
// Logs a formatted informational message to standard error if the log
// level is at least "info"
//
func logInfo(msg string, fmtArgs ...interface{}) {
	p36log.SInfo(msg, fmtArgs...)
}

//
// Logs a formatted debugging message to standard error if the log
// level is at least "debug"
//
func logDebug(msg string, fmtArgs ...interface{}) {
	p36log.SDebug(msg, fmtArgs...)
}

//
// Returns the current Julian calendar day.
//
// Adapted from http://ben-daglish.net/moon.shtml
// Rest In Peace
//
func getJulianDay(date *time.Time) int {
	year := int(date.Year())
	month := int(date.Month()) + 1
	day := int(date.Day())

	if year < 0 {
		year++
	}

	if month <= 2 {
		year--
		month += 12
	}

	ret := int(math.Floor(365.25 * float64(year)) + math.Floor(30.6001 * float64(month)) + float64(day) + 1720995.0)

	if day + 31 * (month + 12 * year) >= (15 + 31 * (10 + 12 * 1582)) {
		ja := int(math.Floor(0.01 * float64(year)))
		ret = ret + 2 - ja + int(math.Floor(0.25 * float64(ja)))
	}

	return ret
}

//
// Returns the fractional part of a floating point number.  In other
// words, the whole part is chopped off.
//
func getFractionalPart(num float64) float64 {
	return num - math.Floor(num)
}
