// Sumomo-Go
// Copyright(C) 2018-2019 Alexa Jones-Gonzales
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/.>
package main

//
// Other Bot Functions
//

import (
	"fmt"
	"math/rand"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
)

type SumomoInstance struct {
	Instance *discordgo.Session
	Conf *Config
	Perms *permsHandler
	ConfigFilename string

	ReminderSys *ReminderSubsystem
	BigmojiSys *BigmojiSubsystem
	//DictSys *DictSubsystem
	WeatherSys *WeatherSubsystem
	LoudnessSys *LoudnessSubsystem
	ServerLog *ServerLogSubsystem

	allCommands CommandTable
}

func NewSumomoInstance() *SumomoInstance {
	ret := &SumomoInstance{
		nil,                        // Instance
		&Config{},                  // Conf
		newPermsHandler(),          // Perms
		"",                         // ConfigFilename

		NewReminderSubystem(),      // ReminderSys
		NewBigmojiSubsystem(),      // BigmojiSys
		//NewDictSubsystem(),         // DictSys
		NewWeatherSubsystem(),      // WeatherSys
		NewLoudnessSubsystem(),     // LoudnessSys
		NewServerLogSubsystem(),    // ServerLog

		make(CommandTable, 0),      // allCommands
	}

	return ret
}

func (bot *SumomoInstance) InitEventHandlers() {
	logInfo("Initializing event handlers\n")

	bot.Instance.AddHandler(bot.onMessageCreate)
	bot.Instance.AddHandler(bot.ServerLog.onMemberJoin)
	bot.Instance.AddHandler(bot.ServerLog.onMemberLeave)
	//Sumomo.Instance.AddHandler(onMessageDelete)
	//Sumomo.Instance.AddHandler(onMessageUpdate)
	bot.Instance.AddHandler(bot.ServerLog.onUserUpdate)
}

//
// Initializes the emoji cache for !bigmoji.
//
func (bot *SumomoInstance) InitSubsystems() error {
	logInfo("Initializing subsystems\n")

	if err := bot.BigmojiSys.Init(bot); err != nil {
		return err
	}

	// if err := bot.DictSys.Init(bot); err != nil {
	// 	return err
	// }

	if err := bot.ReminderSys.Init(bot); err != nil {
		return err
	}

	if err := bot.WeatherSys.Init(bot); err != nil {
		return err
	}

	if err := bot.LoudnessSys.Init(bot); err != nil {
		return err
	}

	if err := bot.ServerLog.Init(bot); err != nil {
		return err
	}

	return nil
}

func (bot *SumomoInstance) InitCommands() {
	logInfo("Initializing commands\n")

	bot.allCommands.registerCommand("!help", "Displays general help", AdminPerm_NotRequired, bot.cmdHelp)
	bot.allCommands.registerCommand("!ping", "Replies with `Pong!`", AdminPerm_NotRequired, bot.cmdPing)

	bot.allCommands.registerCommand("!dice", "Roll custom dice to get random numbers.", AdminPerm_NotRequired,
		bot.cmdDice)

	bot.allCommands.registerCommand("!spell", "Checks the spelling of one or more words.", AdminPerm_NotRequired,
		bot.cmdSpell)

	bot.allCommands.registerCommand("!fortune", "Runs the unix command `fortune` " +
		"(https://en.wikipedia.org/wiki/Fortune_(Unix))", AdminPerm_NotRequired, bot.cmdFortune)

	bot.allCommands.registerCommand("!weather", "Displays the weather for the given area.  " +
		"Use any search string that is valid on OpenWeatherMap.org.",
		AdminPerm_NotRequired, bot.cmdWeather)

	bot.allCommands.registerCommand("!remind", "Schedules a reminder to be PMed to you later.", AdminPerm_NotRequired,
		bot.cmdRemind)

	bot.allCommands.registerCommand("!bigmoji", "Shows an enlarged version of an emoji.", AdminPerm_NotRequired,
		bot.cmdBigmoji)

	bot.allCommands.registerCommand("!decide", "Choose something from a list at random.", AdminPerm_NotRequired,
		bot.cmdDecide)

	//bot.allCommands.registerCommand("!define", "Get the dictionary definition for a word in English", AdminPerm_NotRequired,
	//bot.cmdDefine)

	bot.allCommands.registerCommand("!ddate", "Get the current date according to the Discordian calendar " +
		"(relative to where Sumomo is running)", AdminPerm_NotRequired, bot.cmdDDate)

	bot.allCommands.registerCommand("!poll", "Create a poll for users to vote in", AdminPerm_NotRequired,
		bot.cmdPoll)

	// Admin-only commands
	bot.allCommands.registerCommand("!changenowplaying", "Change Sumomo's \"Now Playing\" status",
		AdminPerm_AnyAdmin, bot.cmdAdminChangeNowPlaying)

	bot.allCommands.registerCommand("!bigavatar", "Shows an enlarged version of a user's avatar",
		AdminPerm_AnyAdmin, bot.cmdBigAvatar)

	bot.allCommands.registerCommand("!displayconfig", "Display Sumomo's current configuration", AdminPerm_AnyAdmin,
		bot.cmdAdminDisplayConfig)


	bot.allCommands.registerCommand("!reloadconfig", "Reloads the configuration file", AdminPerm_MainAdmin,
		bot.cmdAdminReloadConfig)

	bot.allCommands.registerCommand("!reloadluascripts", "Reloads all Lua scripts", AdminPerm_MainAdmin,
		bot.cmdAdminReloadLuaScripts)
}

func (bot *SumomoInstance) NowPlayingHandler(force bool) {
	logInfo("Starting now playing handler\n")

	for {
		if bot.Conf.Global.NowPlayingTime == 0 || len(bot.Conf.Global.NowPlaying) == 0 {
			// Try again soon
			time.Sleep(1 * time.Minute)
			continue
		}

		logDebug("Updating now playing message\n")

		// 1 in 9 chance we don't play a game right now
		if rand.Intn(9) == 5 {
			logDebug("Now playing message set to nothing\n")
			bot.Instance.UpdateStatus(0, "")
		} else {
			num := rand.Intn(len(bot.Conf.Global.NowPlaying))
			str := bot.Conf.Global.NowPlaying[num]
			idle := 0

			logDebug("Now playing message will be set to %q\n", str)

			err := bot.Instance.UpdateStatusComplex(discordgo.UpdateStatusData{
				&idle,
				&discordgo.Game{
					Name: str,
					Type: discordgo.GameTypeGame},
				false,
				""})

			if err != nil {
				logError("While attempting to set the now playing status: %s\n", err)
			}
		}

		if force {
			break
		}

		time.Sleep(time.Duration(bot.Conf.Global.NowPlayingTime) * time.Minute)
	}
}

//
// Returns true if the given user is the main admin user, or false
// otherwise.
//
func (bot *SumomoInstance) userIsMainAdminP(user string) bool {
	if user == bot.Conf.Permissions.Admin {
		return true
	}

	return false
}

//
// Checks to see if the given user is an admin user.  If they are,
// true is returned.  Otherwise false is returned.
//
func (bot *SumomoInstance) userIsAdminP(user string) bool {
	for _, admin := range(bot.Conf.Permissions.OtherAdmins) {
		if user == admin {
			return true
		}
	}

	return bot.userIsMainAdminP(user)
}

//
//
//
func (bot *SumomoInstance) ensureChannel(msg *discordgo.MessageCreate, channelID string, cmdName string) bool {
	ch, err := bot.Instance.Channel(msg.ChannelID)
	chName := ""
	if err != nil {
		logError("Could not find a channel with the ID %s\n", msg.ChannelID)
		chName = "UNKNOWN CHANNEL"
	} else {
		chName = ch.Name
	}

	if msg.ChannelID != channelID {
		logInfo("%q (ID: %q) attempted to use %s in channel %q (ID: %q), which is not allowed\n",
			msg.Author.Username, msg.Author.ID, cmdName, msg.ChannelID, chName)

		dm, err := bot.Instance.UserChannelCreate(msg.Author.ID)
		if err != nil {
			logError("Could not open private message with user %s (ID: %s) to send note about %s\n",
				msg.Author.Username, msg.Author.ID, cmdName)
			return false
		}

		if ch.Type == discordgo.ChannelTypeDM {
			bot.sendMsg(dm.ID, "Sorry, you can't use %s in private channels\n", cmdName)
		} else {
			bot.sendMsg(dm.ID, "Sorry, you can't use %s in the channel #%s\n", cmdName, chName)
		}

		return false
	} else {
		logInfo("%s (ID: %s) is allowed to use %s in channel #%s (ID: %s)\n",
			msg.Author.Username, msg.Author.ID, cmdName, chName, msg.ChannelID)
		return true
	}
}

func (bot *SumomoInstance) sendMsg(channel string, msg string, fmtArgs ...interface{}) {
	bot.Instance.ChannelMessageSend(channel, fmt.Sprintf(msg, fmtArgs...))
}

//
// Sends a formatted private message to the specified user
//
func (bot *SumomoInstance) sendDM(user string, msg string, fmtArgs ...interface{}) {
	dm, err := bot.Instance.UserChannelCreate(user)
	if err != nil {
		logError("Could not open private message with the user %s\n", user)
		return
	}

	bot.sendMsg(dm.ID, fmt.Sprintf("%s\n", fmt.Sprintf(msg, fmtArgs...)))
}

//
// Sends a formatted message to the main admin user.
//
func (bot *SumomoInstance) sendMainAdminDM(msg string, fmtArgs ...interface{}) {
	bot.sendDM(bot.Conf.Permissions.Admin, msg, fmtArgs...)
}

//
// Returns the human-readable name of the given user, or an empty
// string if they aren't found.
//
func (bot *SumomoInstance) getUsernameByID(id string) string {
	settings, err := bot.Instance.User(id)

	if err != nil {
		logError("Error getting username for ID %s: %s\n", id, err)
		return ""
	} else {
		return settings.Username
	}
}

//
// If args has a length of 1 or 0, this will send helpMsg as a
// response to msg.  If the help message is sent, true is returned.
// Otherwise false is returned.
//
func (bot *SumomoInstance) maybeSendHelp(msg *discordgo.MessageCreate, args []string, helpMsg string) bool {
	sendHelp := false
	if len(args) <= 1 {
		sendHelp = true
	} else if len(args) == 2 {
		if strings.ToLower(args[1]) == "--help" || strings.ToLower(args[1]) == "-h" {
			sendHelp = true
		}
	}

	if sendHelp {
		bot.sendMsg(msg.ChannelID, helpMsg)
		return true
	}

	return false
}
