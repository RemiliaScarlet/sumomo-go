// Sumomo-Go
// Copyright(C) 2018-2019 Alexa Jones-Gonzales
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/.>
package main

import (
	"fmt"
	"os"
	"strings"
	"testing"
)

func TestNewPermsHandler(t *testing.T) {
	sumomo := NewSumomoInstance()

	if len(sumomo.Perms.adminOnChannels) != 0 {
		t.Errorf("adminonChannels is not empty\n")
	}

	if len(sumomo.Perms.cmdsAllChannels) != 0 {
		t.Errorf("cmdsAllChannels is not empty\n")
	}

	if len(sumomo.Perms.cmdsAllowed) != 0 {
		t.Errorf("cmdsAllowed is not empty\n")
	}

	if len(sumomo.Perms.cmdsDenied) != 0 {
		t.Errorf("cmdsDenied is not empty\n")
	}

	if len(sumomo.Perms.cmdsNotOnChannels) != 0 {
		t.Errorf("cmdsNotOnChannels is not empty\n")
	}

	if len(sumomo.Perms.cmdsOnChannels) != 0 {
		t.Errorf("cmdsOnChannels is not empty\n")
	}

	if len(sumomo.Perms.mainAdminOnChannels) != 0 {
		t.Errorf("mainAdminOnChannels is not empty\n")
	}
}

func TestPermissionClear(t *testing.T) {
	sumomo := NewSumomoInstance()
	ph := sumomo.Perms

	ph.adminOnChannels["mycommand"] = append(ph.adminOnChannels["mycommand"], "channel1")
	ph.cmdsAllChannels = append(ph.cmdsAllChannels, "list1")
	ph.cmdsAllowed["allowed-command"] = append(ph.cmdsAllowed["allowed-command"], "user1")
	ph.cmdsDenied["denied-command"] = append(ph.cmdsDenied["denied-command"], "notuser1")
	ph.cmdsNotOnChannels["command-not-specific-channel"] = append(ph.cmdsNotOnChannels["command-not-specific-channel"], "channel2")
	ph.cmdsOnChannels["command-specific-channel"] = append(ph.cmdsOnChannels["command-specific-channel"], "channel3")
	ph.mainAdminOnChannels["command-main-admin-chan"] = append(ph.mainAdminOnChannels["command-main-admin-chan"], "chanel36")

	ph.clearPermissions()

	if len(ph.adminOnChannels) != 0 {
		t.Errorf("adminonChannels is not empty\n")
	}

	if len(ph.cmdsAllChannels) != 0 {
		t.Errorf("cmdsAllChannels is not empty\n")
	}

	if len(ph.cmdsAllowed) != 0 {
		t.Errorf("cmdsAllowed is not empty\n")
	}

	if len(ph.cmdsDenied) != 0 {
		t.Errorf("cmdsDenied is not empty\n")
	}

	if len(ph.cmdsNotOnChannels) != 0 {
		t.Errorf("cmdsNotOnChannels is not empty\n")
	}

	if len(ph.cmdsOnChannels) != 0 {
		t.Errorf("cmdsOnChannels is not empty\n")
	}

	if len(ph.mainAdminOnChannels) != 0 {
		t.Errorf("mainAdminOnChannels is not empty\n")
	}
}

func TestPermsPropogateGoodConfig(t *testing.T) {
	sumomo := NewSumomoInstance()
	sumomo.InitCommands()

	in := strings.NewReader(fakeGoodConfig)
	conf, err := parseConfigStream(in)
	sumomo.Conf = conf

	if err != nil {
		t.Errorf("Config parsing error: %s\n", err)
	} else if sumomo.Conf == nil {
		t.Errorf("Config parsing error, conf was nil\n")
	}

	if !sumomo.Perms.populate(sumomo, sumomo.Conf) {
		t.Errorf("Could not populate permsHandler\n")
	}
}

func TestPermsPropogateBadPermissionsSection(t *testing.T) {
	fmt.Fprintf(os.Stderr, "You will see an error message in this test, ignore it\n")
	sumomo := NewSumomoInstance()
	sumomo.InitCommands()

	in := strings.NewReader(fakeGoodConfig)
	conf, err := parseConfigStream(in)
	sumomo.Conf = conf

	if err != nil {
		t.Errorf("Config parsing error: %s\n", err)
	} else if sumomo.Conf == nil {
		t.Errorf("Config parsing error, conf was nil\n")
	}

	sumomo.Conf.Permissions.Commands["lol"] = []string{"nope"}
	sumomo.Conf.Permissions.Commands["help"] = []string{"invalid"}
	sumomo.Conf.Permissions.Commands["ping"] = []string{"allow", "bad"}

	if sumomo.Perms.populate(sumomo, sumomo.Conf) {
		t.Errorf("Should not have populated permsHandler\n")
	}
}

func TestAdminPredicates(t *testing.T) {
	sumomo := NewSumomoInstance()
	sumomo.InitCommands()

	in := strings.NewReader(fakeGoodConfig)
	conf, err := parseConfigStream(in)
	sumomo.Conf = conf
	sumomo.Perms.populate(sumomo, conf)

	if err != nil {
		t.Errorf("Config parsing error: %s\n", err)
	} else if conf == nil {
		t.Errorf("Config parsing error, conf was nil\n")
	}

	//
	// Any Admin testing
	//
	if sumomo.userIsAdminP("69") {
		t.Errorf("userIsAdminP(\"69\") should have returned false\n")
	}

	if !sumomo.userIsAdminP("1234567890") {
		t.Errorf("userIsAdminP(\"1234567890\") should have returned true\n")
	}

	if !sumomo.userIsAdminP("9876543210") {
		t.Errorf("userIsAdminP(\"9876543210\") should have returned true\n")
	}

	//
	// Main Admin testing
	//

	if sumomo.userIsMainAdminP("69") {
		t.Errorf("userIsMainAdminP(\"69\") should have returned false\n")
	}

	if !sumomo.userIsMainAdminP("1234567890") {
		t.Errorf("userIsMainAdminP(\"1234567890\") should have returned true\n")
	}

	if sumomo.userIsMainAdminP("9876543210") {
		t.Errorf("userIsMainAdminP(\"9876543210\") should have returned false\n")
	}
}
