// Sumomo-Go
// Copyright(C) 2018-2019 Alexa Jones-Gonzales
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/.>
package main

//
// Main entry point and command line handling
//

import (
	"fmt"
	"math/rand"
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"strings"
	"syscall"
	"time"

	"scarletdevilmansion.tech/go/argparser-go"
	"scarletdevilmansion.tech/go/p36lib-go"
	"scarletdevilmansion.tech/go/p36log-go/v2"

	"github.com/bwmarrin/discordgo"
)

/*==============================================================================
 * Functions
 *============================================================================*/

//
// Sets up a command line parser and returns it
//
func setupCommandLine() *argparser.ArgParser {
	args := argparser.NewArgParser(programName, programVersion)

    args.PostVersionText = `
Copyright (C) 2018-2019 Alexa Jones-Gonzales
This is free software.  You may redistribute copies of it under the terms of
the GNU General Public License <http://www.gnu.org/licenses/gpl.html>.
There is NO WARRANTY, to the extent permitted by law.`

    ////////////////////////////////////
    // Argument definitions
    ///////////////////////////////////

	args.AddArgument("config", 'f', argparser.AT_String, nil, "",
		"The configuration file to read (required)")

	args.AddArgument("test-config", 'T', argparser.AT_Flag, nil, "",
		"Exit after the configuration file is parsed and printed.")

	args.AddArgument("admin", 'a', argparser.AT_String, nil, "",
		"The numeric ID of the admin user to ping when something goes wrong")

	args.AddArgument("dont-reconnect", '\000', argparser.AT_Flag, nil, "",
		"Don't attempt to reconnect if disconnected.")

	args.AddArgument("log-level", 'l', argparser.AT_String, nil, "",
		"Controls the amount of verbosity for some output logging.  Can be, in increasing verbosity: 'error', 'warning', 'info' or 'debug'.")

	args.AddArgument("bot-log-level", '\000', argparser.AT_String, nil, "",
		"Controls the amount of verbosity for the discordgo library.  This is seprate from what Sumomo-Go itself will output.  Can be, in increasing verbosity: 'error', 'warning', 'info' or 'debug'.")


	args.AddArgument("bigmoji-size", '\000', argparser.AT_Int, int64(192), "bigmoji",
		"Specify the width of emojis resized with the !bigmoji command.  The height of the images will be adjusted to preserve aspect ratio.  Default: 192")

	args.AddArgument("bigmoji-cache-limit", '\000', argparser.AT_Int, int64(20), "bigmoji",
		"The maximum number of cached emoji to keep in memory.  Default: 20")

	args.AddArgument("bigmoji-cache-time", '\000', argparser.AT_Int, int64(30), "bigmoji",
		"The maximum number of seconds to keep old emoji in memory.  Default: 30 (minimum: 30)")


	args.AddArgument("image-filter", '\000', argparser.AT_String, "nearest", "Images",
		"Controls the type of filtering used when resizing images.  Can be 'nearest', 'bspline', 'bartlett', 'blackman', 'box', catmullrom', 'cosine', 'gaussian', 'hamming', 'hann', hermite', 'lanczos', 'linear', 'mitchellnetravali', or 'welch'.  The default is 'nearest'.")


	args.AddArgument("reminders", 'r', argparser.AT_String, nil, "Databases",
		"The Sqlite database holding reminder information.")

    args.AddArgument("token", 't', argparser.AT_String, nil, "Authentication",
		"The authentication token for Discord.")
	args.AddArgument("weather", '\000', argparser.AT_String, nil, "Authentication",
		"The OpenWeatherMap.org API key to use.")

	args.AddArgument("include-forecasts", '\000', argparser.AT_Flag, nil, "Weather Options",
		"Retrieve 16-day daily forecasts as well.")

	args.AddArgument("forecast-len", '\000', argparser.AT_Int, int64(3), "Weather Options",
		"The number of days to include in the forecast.  Must be between 1 and 16.  Default: 3)")

	args.AddArgument("lua-directory", '\000', argparser.AT_String, nil, "Scripting",
		"Adds all Lua scripts from the given directory and its subdirectories.  The directory and its subdirectories should ONLY have Lua scripts in them.")
	return args
}

//
// Checks that the loaded configuration in Sumomo.Conf is valid, and
// prints any messages to standard error.  If the configuration is
// indeed valid, this returns true.  Otherwise this returns false.
//
func doCmdLineConfigCheck(sumomo *SumomoInstance) bool {
	errs, ok := sumomo.Conf.checkConfiguration()
	out := p36lib.StderrColored
	ret := true

	errs.UseColor = true
	errs.UseWrapping = true
	errs.Output = out

	if !ok {
		ret = false

		if len(errs.Errors) == 0 {
			panic("Length of errors should not be 0 when ok is false")
		} else {
			errs.printErrs("Config Errors")
			fmt.Fprintf(out, "\n")
		}
	} else if ok && len(errs.Errors) != 0 {
		panic("Length of errors should be 0 when ok is true")
	}


	errs.printWarns("Config Warnings")
	fmt.Fprintf(out, "\n")

	errs.printNotes("Config Notes")
	fmt.Fprintf(out, "\n")

	if ret {
		p36lib.FormatColored(p36lib.FormatWhiteBold, "** Configuration looks good **\n")
	} else {
		p36lib.FormatColored(p36lib.FormatRedBold, "** Configuration errors encountered, cannot continue **\n")
	}

	return ret
}

/*==============================================================================
 * Main entry point
 *============================================================================*/

func main() {
	var err error
	args := setupCommandLine()

	rand.Seed(time.Now().UnixNano())

    if err := args.Parse(os.Args, true, true); err != nil {
		die("%s\n", err)
    }

	// Initialize the main Sumomo instance.  This must be done before
	// the configuration file is parsed since permission-related bits
	// of the config gets stored in here.
	sumomo := NewSumomoInstance()
	sumomo.InitCommands()

	// Read configuration file
	if arg, err := args.GetArg("--config"); err != nil {
		die("%s\n", err)
	} else if !args.ArgCalledP("--config") || arg.GetString() == "" {
		die("No configuration file specified\n")
	} else {
		sumomo.ConfigFilename = arg.GetString()

		if newCfg, err := parseConfigFile(sumomo.ConfigFilename); err != nil {
			die("Error parsing configuration file: %s\n", err)
		} else {
			sumomo.Conf = newCfg
		}
	}

	// Populate the permissions
	if !sumomo.Perms.populate(sumomo, sumomo.Conf) {
		die("Error with permissions section of configuration file\n")
	}

	//
	// Handle the rest of the command line arguments.  Some might
	// override what's in the configuration file.
	//

	if arg, err := args.GetArg("--admin"); err != nil {
		die("%s\n", err)
	} else if args.ArgCalledP("--admin") {
		sumomo.Conf.Permissions.Admin = arg.GetString()
		fmt.Printf("Setting admin user from command line: %s\n", sumomo.Conf.Permissions.Admin)
	}

	if arg, err := args.GetArg("--token"); err != nil {
		die("%s\n", err)
	} else if args.ArgCalledP("--token") {
		sumomo.Conf.Global.Token = arg.GetString()
		fmt.Printf("Setting Discord token from command line\n")
	}

	if arg, err := args.GetArg("--weather"); err != nil {
		die("%s\n", err)
	} else if args.ArgCalledP("--weather") {
		sumomo.Conf.Weather.Key = arg.GetString()
		fmt.Printf("Setting weather API key from command line\n")
	}

	if arg, err := args.GetArg("--include-forecasts"); err != nil {
		die("%s\n", err)
	} else if args.ArgCalledP("--include-forecasts") {
		sumomo.Conf.Weather.IncludeForecasts = arg.GetFlag()
		fmt.Printf("Overriding includeforecasts to true from command line\n")
	}

	if arg, err := args.GetArg("--forecast-len"); err != nil {
		die("%s\n", err)
	} else if args.ArgCalledP("--forecast-len") {
		val := arg.GetInt()
		sumomo.Conf.Weather.NumForecastDays = int(val)
		fmt.Printf("Setting number of forecast days from command line: %d\n", sumomo.Conf.Weather.NumForecastDays)
	}

	if arg, err := args.GetArg("--bigmoji-size"); err != nil {
		die("%s\n", err)
	} else if args.ArgCalledP("--bigmoji-size") {
		num := arg.GetInt()
		sumomo.Conf.Bigmoji.Resize = int(num)
		fmt.Printf("Setting bigmoji size from command line: %d\n", sumomo.Conf.Bigmoji.Resize)
	}

	if arg, err := args.GetArg("--bigmoji-cache-limit"); err != nil {
		die("%s\n", err)
	} else if args.ArgCalledP("--bigmoji-cache-limit") {
		num := arg.GetInt()
		sumomo.Conf.Bigmoji.CacheLimit = int(num)
		fmt.Printf("Setting bigmoji cache limit from command line: %d\n", sumomo.Conf.Bigmoji.CacheLimit)
	}

	if arg, err := args.GetArg("--bigmoji-cache-time"); err != nil {
		die("%s\n", err)
	} else if args.ArgCalledP("--bigmoji-cache-time") {
		num := arg.GetInt()
		if num < 30 {
			die("--bigmoji-cache-time must be at least 30\n")
		} else {
			sumomo.Conf.Bigmoji.CacheLimit = int(num)
			fmt.Printf("Setting bigmoji cache time limit from command line: %d\n", sumomo.Conf.Bigmoji.CacheLimit)
		}
	}

	if arg, err := args.GetArg("--image-filter"); err != nil {
		die("%s\n", err)
	} else if args.ArgCalledP("--image-filter") {
		sumomo.Conf.Images.Filter = strings.ToLower(arg.GetString())
		fmt.Printf("Setting image filter from command line: %s\n", sumomo.Conf.Images.Filter)
	}

	if arg, err := args.GetArg("--log-level"); err != nil {
		die("%s\n", err)
	} else if args.ArgCalledP("--log-level") {
		sumomo.Conf.Global.LogLevel = strings.ToLower(arg.GetString())
		fmt.Printf("Setting log level from command line: %s\n", sumomo.Conf.Global.LogLevel)
	}

	if arg, err := args.GetArg("--bot-log-level"); err != nil {
		die("%s\n", err)
	} else if args.ArgCalledP("--bot-log-level") {
		sumomo.Conf.Global.BotLogLevel = strings.ToLower(arg.GetString())
		fmt.Printf("Setting bot log level from command line: %s\n", sumomo.Conf.Global.BotLogLevel)
	}

	if arg, err := args.GetArg("--lua-directory"); err != nil {
		die("%s\n", err)
	} else if args.ArgCalledP("--lua-directory") {
		sumomo.Conf.Global.LuaDirectory = arg.GetString()
		fmt.Printf("Setting Lua directory from command line: %s\n", sumomo.Conf.Global.LuaDirectory)
	}

	// Report configuration, then check for validity
	sumomo.Conf.reportConfig()
	if !doCmdLineConfigCheck(sumomo) {
		os.Exit(64)
	}

	// Setup the log level in p36log.  This is separate from the bot
	// logging.
	if sumomo.Conf.Global.LogLevel == "error" {
		p36log.SimpleErrorEnabled = true
		p36log.SimpleWarningEnabled = false
		p36log.SimpleInfoEnabled = false
		p36log.SimpleDebugEnabled = false
	} else if sumomo.Conf.Global.LogLevel == "warning" {
		p36log.SimpleErrorEnabled = true
		p36log.SimpleWarningEnabled = true
		p36log.SimpleInfoEnabled = false
		p36log.SimpleDebugEnabled = false
	} else if sumomo.Conf.Global.LogLevel == "info" {
		p36log.SimpleErrorEnabled = true
		p36log.SimpleWarningEnabled = true
		p36log.SimpleInfoEnabled = true
		p36log.SimpleDebugEnabled = false
	} else if sumomo.Conf.Global.LogLevel == "debug" {
		p36log.SimpleErrorEnabled = true
		p36log.SimpleWarningEnabled = true
		p36log.SimpleInfoEnabled = true
		p36log.SimpleDebugEnabled = true
	}

	// Finish setting up p36-log-go
	p36log.SimpleErrorHeader = "Sumomo/Error"
	p36log.SimpleWarningHeader = "Sumomo/Warning"
	p36log.SimpleInfoHeader = "Sumomo/Info"
	p36log.SimpleDebugHeader = "Sumomo/Debug"

	//
	// Setup scripting
	//
	if sumomo.Conf.Global.LuaDirectory != "" {
		luaDirErr := filepath.Walk(sumomo.Conf.Global.LuaDirectory, func(filename string, fInfo os.FileInfo, fError error) error {
			cmdName := filepath.Base(filename)
			ext := filepath.Ext(filename)

			cmdName = "!" + cmdName[:strings.LastIndex(cmdName, ext)]

			if !fInfo.IsDir() {
				if proto, err := loadLuaFile(filename); err != nil {
					die("%s\n", err)
				} else if err = luaCommands.registerLuaCommand(cmdName, proto); err != nil {
					die("%s\n", err)
				}
			}

			return nil
		})

		if luaDirErr != nil {
			die("Error walking lua directory: %s\n", luaDirErr)
		}
	}

	//
	// Check for --test-config
	//
	if arg, err := args.GetArg("--test-config"); err != nil {
		die("%s\n", err)
	} else if arg.GetFlag() {
		fmt.Printf("** Configuration file successfully parsed.  Exiting due to --test-config **\n")
		os.Exit(0)
	}

	//
	// Startup the bot
	//

	sumomo.Instance, err = discordgo.New("Bot " + sumomo.Conf.Global.Token)

	if err != nil {
		die("%s\n", err)
	}

	// Setup the log level for the bot itself
	if sumomo.Conf.Global.BotLogLevel == "error" {
		sumomo.Instance.LogLevel = discordgo.LogError
	} else if sumomo.Conf.Global.BotLogLevel == "warning" {
		sumomo.Instance.LogLevel = discordgo.LogWarning
	} else if sumomo.Conf.Global.BotLogLevel == "info" {
		sumomo.Instance.LogLevel = discordgo.LogInformational
	} else if sumomo.Conf.Global.BotLogLevel == "debug" {
		sumomo.Instance.LogLevel = discordgo.LogDebug
	}

	// Finish setting up p36-log-go
	logInfo("Bot loging level set to %s\n", sumomo.Conf.Global.BotLogLevel)

	// Init emoji cache
	if err := sumomo.InitSubsystems(); err != nil {
		die("%s\n", err)
	}

	sumomo.Instance.State.MaxMessageCount = 100

	sumomo.InitEventHandlers()

	// Start the bot
	if err := sumomo.Instance.Open(); err != nil {
		die("Could not connect: %s\n", err)
	}
	defer sumomo.Instance.Close()

	go func() {
		time.Sleep(9 * time.Second)
		sumomo.NowPlayingHandler(false)
	}()

	//
	// Startup the reminder system
	//
	go func() {
		time.Sleep(time.Second)
		go sumomo.ReminderLoop(sumomo.ReminderSys)
	}()

	// Force a GC now
	runtime.GC()

	// Handle Ctrl+C
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sigChan
}
