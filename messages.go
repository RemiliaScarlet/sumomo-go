// Sumomo-Go
// Copyright(C) 2018-2019 Alexa Jones-Gonzales
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/.>
package main

//
// Message Handling
//

import (
	"strings"

	"github.com/bwmarrin/discordgo"
)

//
// Handler for when a user sends a message
//
func (sumomo *SumomoInstance) onMessageCreate(bot *discordgo.Session, msg *discordgo.MessageCreate) {
	// Ignore our own messages
	if msg.Author.ID == sumomo.Instance.State.User.ID {
		return
	}

	if bot == nil {
		sumomo.sendMainAdminDM("Got nil bot in onMessageCreate?  Message ID: %v\n", msg.ID)
		logError("Got nil bot in onMessageCreate?  Message ID: %v\n", msg.ID)
		return
	}

	if ch, _ := sumomo.Instance.Channel(msg.ChannelID); ch == nil {
		sumomo.sendMainAdminDM("Got nil channel in onMessageCreate?  Message ID: %v\n", msg.ID)
		logError("Got nil channel in onMessageCreate?  Message ID: %v\n", msg.ID)
		return
	} else if ch.Type == discordgo.ChannelTypeGuildText {
		go sumomo.LoudnessSys.HandleLoudPosters(msg)
	}

	if len(msg.Mentions) > 0 {
		mentions := make([]*discordgo.User, len(msg.Mentions), len(msg.Mentions))
		copy(mentions, msg.Mentions)
		go sumomo.ServerLog.logMentions(msg.GuildID, msg.Author, msg.ChannelID, mentions)
	}

	if strings.TrimSpace(msg.Content) == "ls -l" {
		sumomo.sendMsg(msg.ChannelID, "This isn't a terminal window, silly :-P")
		return
	}

	args := splitAsArgs(msg.Content)
	logDebug("Message received: \"%q\"\n", args)

	if len(args) >= 1 {
		cmd := args[0]

		if cmdObj := sumomo.allCommands.findCommand(cmd); cmdObj != nil {
			if cmdObj.needsAdmin == AdminPerm_AnyAdmin {
				if sumomo.userIsAdminP(msg.Author.ID) {
					cmdObj.function(msg, args)

				} else {
					logInfo("User %s attempted to run admin-only command %s\n", msg.Author.ID, cmdObj.name)
				}

			} else if cmdObj.needsAdmin == AdminPerm_MainAdmin {
				if sumomo.userIsMainAdminP(msg.Author.ID) {
					cmdObj.function(msg, args)
				} else {
					logInfo("User %s attempted to run a command meant only for the main admin %s\n", msg.Author.ID, cmdObj.name)
				}
			} else {
				cmdObj.function(msg, args)
			}

			return
		}

		if luaCmd := luaCommands.findLuaCommand(cmd); luaCmd != nil {
			state, err := getLuaState()
			defer state.Close()

			if err != nil {
				sumomo.sendMainAdminDM("Couldn't get lua state for command %q: %v\n", cmd, err)
			}

			if err = sumomo.callLuaCommand(luaCmd, state, msg, args); err != nil {
				sumomo.sendMainAdminDM("Couldn't run command %q: %v\n", cmd, err)
			}
		}
	}
}
