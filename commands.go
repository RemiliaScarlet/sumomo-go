// Sumomo-Go
// Copyright(C) 2018-2019 Alexa Jones-Gonzales
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/.>
package main

//
// Various basic bot commands.  More involved commands get their own
// source file.
//

import (
	"bytes"
	"fmt"
	"image"
	"image/png"
	"io/ioutil"
	"math/rand"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/disintegration/imaging"
)

/*==============================================================================
 * Types
 *============================================================================*/

type BuiltInAdminPerm int
const (
	AdminPerm_AnyAdmin BuiltInAdminPerm = iota
	AdminPerm_MainAdmin
	AdminPerm_NotRequired
)

type commandFunc func(msg *discordgo.MessageCreate, args []string)

type command struct {
	name string
	description string
	needsAdmin BuiltInAdminPerm
	function commandFunc
}

type CommandTable map[string]*command

/*==============================================================================
 * Command Registration
 *============================================================================*/

//
// Registers a new command with the given name and description, as
// well as a commandFunc to run when the command is called.  If a
// duplicate command is encountered, this will panic()
//
func (tbl CommandTable) registerCommand(name, desc string, adminOnly BuiltInAdminPerm, fnc commandFunc) {
	if _, found := tbl[name]; found {
		panic("Duplicate command: " + name)
	} else {
		tbl[name] = &command{name, desc, adminOnly, fnc}
	}
}

/*==============================================================================
 * CommandsTable
 *============================================================================*/

//
// Searches for a command by name.  Returns the command struct
// associated with it if it was found, or nil otherwise.  This will
// not prepend the exclamation point on the command name, so you are
// expected to pass a string with that already added.
//
func (tbl CommandTable) findCommand(cmd string) *command {
	if cmdObj, found := tbl[cmd]; found {
		return cmdObj
	} else {
		return nil
	}
}

/*==============================================================================
 * Basic Bot Commands
 *============================================================================*/

//
// A special commandFunc that is used when a command is not yet
// implemented
//
func (bot *SumomoInstance) cmdNotImplemented(msg *discordgo.MessageCreate, args []string) {
	bot.sendMsg(msg.ChannelID, "Sorry, not yet implemented")
}

//
// Implements the !ping command
//
func (bot *SumomoInstance) cmdPing(msg *discordgo.MessageCreate, args []string) {
	bot.sendMsg(msg.ChannelID, "Pong!")
}

//
// Implements the !help command.  The help message is generated from
// all other registered commands.
//
func (bot *SumomoInstance) cmdHelp(msg *discordgo.MessageCreate, args []string) {
	ret := "__**Sumomo-Bot Commands:**__\n\n"

	for _, cmd := range(bot.allCommands) {
		if cmd.needsAdmin == AdminPerm_NotRequired {
			ret = ret + "**" + cmd.name + "**: " + cmd.description + "\n"
		}
	}

	for _, cmd := range(luaCommands) {
		if cmd.needsAdmin == AdminPerm_NotRequired {
			ret = ret + "**" + cmd.name + "**: " + cmd.description + "\n"
		}
	}

	ret += "\nTo get help on a specific command, invoke Sumomo-Bot like this:\n" +
		"`![your command here] --help`\n\n" +
		"For example: `!ping --help`\n"

	bot.sendMsg(msg.ChannelID, "%s", ret)

	// Also include AdminPerm_MainAdmin commands for the main admin user
	if bot.userIsMainAdminP(msg.Author.ID) && msg.ChannelID == bot.Conf.Permissions.AdminControlChannel {
		ret := "__**Sumomo-Bot Commands (Main Admin Only):**__\n"

		for _, cmd := range(bot.allCommands) {
			if cmd.needsAdmin == AdminPerm_MainAdmin {
				ret = ret + "**" + cmd.name + "**: " + cmd.description + "\n"
			}
		}

		for _, cmd := range(luaCommands) {
			if cmd.needsAdmin == AdminPerm_MainAdmin {
				ret = ret + "**" + cmd.name + "**: " + cmd.description + "\n"
			}
		}

		bot.sendMsg(msg.ChannelID, "%s", ret)
	}

	// Also include AdminPerm_AnyAdmin commands for admin users
	if bot.userIsAdminP(msg.Author.ID) && msg.ChannelID == bot.Conf.Permissions.AdminControlChannel {
		ret := "__**Sumomo-Bot Commands (Admins Only):**__\n\n"

		for _, cmd := range(bot.allCommands) {
			if cmd.needsAdmin == AdminPerm_AnyAdmin {
				ret = ret + "**" + cmd.name + "**: " + cmd.description + "\n"
			}
		}

		for _, cmd := range(luaCommands) {
			if cmd.needsAdmin == AdminPerm_AnyAdmin {
				ret = ret + "**" + cmd.name + "**: " + cmd.description + "\n"
			}
		}

		bot.sendMsg(msg.ChannelID, "%s", ret)
	}
}

//
// Implements the !dice command
//
func (bot *SumomoInstance) cmdDice(msg *discordgo.MessageCreate, args []string) {
	logInfo("Asked for dice rolls: %s\n", args)

	var numDice uint64
	var numSide uint64
	var ret strings.Builder

	if bot.maybeSendHelp(msg, args, `Usage: **!dice** _<die specifier>_

A die specifier can be something like 1d6 (for a single 6-sided die), 2d10 (two 10-sided die), 42d69 (fourty-two 69-sided dice), etc.`) {
	    return
    }


	// Parse out the dice specifier (XdY, where X and Y are numbers, like 1d4 or 20d6)
	re := regexp.MustCompile(`^([0-9]+)d([0-9]+)$`)
	matches := re.FindStringSubmatch(args[1])

	if len(matches) != 3 {
		bot.sendMsg(msg.ChannelID, "Sorry, but I can't understand that dice specifier.")
		return
	}

	// Check that the number of dice and sides are valid
	if parsed, err := strconv.ParseUint(matches[1], 10, 16); err != nil {
		bot.sendMsg(msg.ChannelID, "Sorry, bad number of dice specified.  This must be a number between 1 and 256.")
		return
	} else if parsed == 0 {
		bot.sendMsg(msg.ChannelID, "Umm, I can't roll zero dice.  Unless you just want me to say 0...")
		return
	} else if parsed > 256 {
		bot.sendMsg(msg.ChannelID, "Sorry, bad number of dice specified.  This must be a number between 1 and 256.")
		return
	} else {
		numDice = parsed
	}

	if parsed, err := strconv.ParseUint(matches[2], 10, 16); err != nil {
		bot.sendMsg(msg.ChannelID, "Sorry, bad number of sides specified.  This must be a number between 3 and 256.")
		return
	} else if parsed == 0 {
		bot.sendMsg(msg.ChannelID,
			"What do you want me to do, change the properties of the physical universe so that zero-sided dice exist?.")
		return
	} else if parsed == 1 {
		bot.sendMsg(msg.ChannelID, "Ehh.... What does a one-sided die look like?")
		return
	} else if parsed > 256 {
		bot.sendMsg(msg.ChannelID, "Sorry, bad number of sides specified.  This must be a number between 3 and 256.")
		return
	} else {
		numSide = parsed
	}

	// If we have just two sides, we can just "flip a coin"
	if numSide == 2 {
		coins := make([]string, numDice, numDice)

		for i := 0; uint64(i) < numDice; i++ {
			num := rand.Intn(10001) // 1 in 10,000 chance of landing on its edge

			if num < 5000 {
				coins[i] = "Heads"
			} else if num >= 5000 && num < 10000 {
				coins[i] = "Tails"
			} else {
				coins[i] = "this one landed on its edge -_-"
			}
		}

		plural := "coins"

		if numDice == 1 {
			plural = "coin"
		} else if numDice == 69 {
			plural = "kinky coins"
		}

		// Build the return string
		ret.WriteString(fmt.Sprintf("Ok, let's see what we get... *flips %d %s*\n", numDice, plural))
		for i, coin := range(coins) {
			ret.WriteString(fmt.Sprintf("%s", coin))

			if i < (len(coins) - 1) {
				ret.WriteString(", ")
			}
		}

		retStr := ret.String()

		// Return the data
		if len(retStr) >= 2000 {
			bot.sendMsg(msg.ChannelID,
				"Uh oh... too much data!  The resulting message is too long.  Try again with a smaller number of coins.")
		} else {
			bot.sendMsg(msg.ChannelID, "%s", retStr)
		}
	} else {
		// If we're here, we're rolling actual dice, not flipping a
		// coin.  Start by rolling the dice and getting the total of
		// all the rolls.
		dice := make([]uint8, numDice, numDice)
		var total uint64

		for i := 0; uint64(i) < numDice; i++ {
			dice[i] = uint8(rand.Intn(int(numSide)) + 1)
			total += uint64(dice[i])
		}

		plural := "dice"
		if numDice == 1 {
			if numSide == 69 {
				plural = "kinky die"
			} else {
				plural = "die"
			}
		} else if numSide == 69 {
			plural = "kinky dice"
		}

		// Build the return string
		ret.WriteString(fmt.Sprintf("Ok, let's see what we get... " +
			"*rolls her %d %s with %d sides like a professional D&D player*\n",
			numDice, plural, numSide))
		for i, die := range(dice) {
			ret.WriteString(fmt.Sprintf("%d", die))

			if i < (len(dice) - 1) {
				ret.WriteString(", ")
			}
		}

		ret.WriteString(fmt.Sprintf("\n\n**Total:** %d", total))
		retStr := ret.String()

		// Return the data
		if len(retStr) >= 2000 {
			bot.sendMsg(msg.ChannelID, "Uh oh... too much data!  The resulting message is too long.  " +
				"Try again with a smaller number of dice and/or sides.")
		} else {
			bot.sendMsg(msg.ChannelID, "%s", retStr)
		}
	}
}

//
// Implements the !fortune command
//
func (bot *SumomoInstance) cmdFortune(msg *discordgo.MessageCreate, args []string) {
	if fortune, err := exec.LookPath("fortune"); err != nil {
		bot.sendMainAdminDM("Uh oh, I couldn't find the `fortune` command")
		bot.sendMsg(msg.ChannelID, "Whoops!  Something went wrong.  I notified the main admin about it.\n")
		return
	} else {
		cmd := exec.Command(fortune)
		out, _ := cmd.StdoutPipe()

		if err != nil {
			bot.sendMainAdminDM("Uh oh, I had problems running `fortune`.")
			bot.sendMsg(msg.ChannelID, "Whoops!  Something went wrong.  I notified the main admin about it.\n")
			return
		}

		if err = cmd.Start(); err != nil {
			bot.sendMainAdminDM("Uh oh, I couldn't run `fortune`.")
			bot.sendMsg(msg.ChannelID, "Whoops!  Something went wrong.  I notified the main admin about it.\n")
			return
		}

		buf, _ := ioutil.ReadAll(out)

		if err = cmd.Wait(); err != nil {
			bot.sendMainAdminDM("Uh oh, `fortune` had a problem.")
			bot.sendMsg(msg.ChannelID, "Whoops!  Something went wrong.  I notified the main admin about it.\n")
			return
		}

		bot.sendMsg(msg.ChannelID, "%s", string(buf))
	}
}

//
// Implements the !ddate command
//
func (bot *SumomoInstance) cmdDDate(msg *discordgo.MessageCreate, args []string) {
	if ddate, err := exec.LookPath("ddate"); err != nil {
		bot.sendMainAdminDM("Uh oh, I couldn't find the `ddate` command")
		bot.sendMsg(msg.ChannelID, "Whoops!  Something went wrong.  I notified the main admin about it.\n")
		return
	} else {
		cmd := exec.Command(ddate)
		out, _ := cmd.StdoutPipe()

		if err != nil {
			bot.sendMainAdminDM("Uh oh, I had problems running `ddate`.")
			bot.sendMsg(msg.ChannelID, "Whoops!  Something went wrong.  I notified the main admin about it.\n")
			return
		}

		if err = cmd.Start(); err != nil {
			bot.sendMainAdminDM("Uh oh, I couldn't run `ddate`.")
			bot.sendMsg(msg.ChannelID, "Whoops!  Something went wrong.  I notified the main admin about it.\n")
			return
		}

		buf, _ := ioutil.ReadAll(out)

		if err = cmd.Wait(); err != nil {
			bot.sendMainAdminDM("Uh oh, `ddate` had a problem.")
			bot.sendMsg(msg.ChannelID, "Whoops!  Something went wrong.  I notified the main admin about it.\n")
			return
		}

		bot.sendMsg(msg.ChannelID, "%s\n(relative to where Sumomo is running)", string(buf))
	}
}

//
// Implements the !spell command
//
func (bot *SumomoInstance) cmdSpell(msg *discordgo.MessageCreate, args []string) {
	logInfo("Asked for spellig help: %s\n", args)

	if bot.maybeSendHelp(msg, args, `Usage: **!spell** _<word>_`) {
	    return
    }

	var aspell string
	var err error
	var out bytes.Buffer
	var errOut bytes.Buffer

	if aspell, err = exec.LookPath("aspell"); err != nil {
		logError("Could not find aspell command\n")
		bot.sendMainAdminDM("Uh oh, I couldn't find the `aspell` command.")
		bot.sendMsg(msg.ChannelID, "Whoops!  Something went wrong.  I notified the main admin about it.\n")
		return
	}

	cmd := exec.Command(aspell, "-a")
	cmd.Stdin = bytes.NewBufferString(args[1])
	cmd.Stdout = &out
	cmd.Stderr = &errOut

	if err = cmd.Run(); err != nil {
		logError("Failed to run aspell: %s\n", err)
		logError("aspell stderr: %s\n", errOut.String())
		bot.sendMainAdminDM("Uh oh, I couldn't run `aspell`.")
		bot.sendMsg(msg.ChannelID, "Whoops!  Something went wrong.  I notified the main admin about it.\n")
		return
	}

	ret := ""
	for _, mistake := range(strings.Split(out.String(), "&")) {
		mistake = strings.TrimSpace(mistake)
		if colonPos := strings.IndexRune(mistake, ':'); colonPos > 0 {
			firstSpace := strings.Index(mistake, " ")

			ret += fmt.Sprintf("**=> %s might be misspelled.**\nSuggestions: %s",
				mistake[:firstSpace], mistake[colonPos:])
		}
	}

	extraRet := ""
	if len(args) != 2 {
		extraRet = "\n\nbtw... I can only look at one word at a time (^.~)"
	}

	if ret == "" {
		bot.sendMsg(msg.ChannelID, "Everything luks like its spleled correctely.%s", extraRet)
	} else {
		bot.sendMsg(msg.ChannelID, "%s%s", ret, extraRet)
	}
}

//
// Implements the !decide command
//
func (bot *SumomoInstance) cmdDecide(msg *discordgo.MessageCreate, args []string) {
	logInfo("Asking for help deciding: %s\n", args)

	if bot.maybeSendHelp(msg, args, "Usage: **!decide** _<list of things>_\n\n" +
		"The _<list of things>_ should be separated by semicolons.  So for example, if you're trying to decide " +
		"what to drink, you would run `!decide tea; coffee; milk; sake; water, lemonade`") {
		return
	}

	// Lists can be delimited by either semicolons or commas
	list := strings.Split(strings.Join(args[1:], " "), ";")
	if len(list) == 1 {
		list = strings.Split(strings.Join(args[1:], " "), ",")
	}

	// Filter out empty strings
	newList := make([]string, 0, 0)
	for _, str := range(list) {
		if strings.TrimSpace(str) != "" {
			newList = append(newList, str)
		}
	}
	list = newList

	if len(list) == 0 {
		bot.sendMsg(msg.ChannelID, "Uhh... umm... I dunno, I can't decide (^ڡ^)",)
		return
	}

	if len(list) == 1 {
		bot.sendMsg(msg.ChannelID, "Gee, I dunno, that's a hard one.  So many options to choose from!  Maybe _%s_? (^ڡ^)",
			list[0])
		return
	}

	// Do the decision
	num := rand.Intn(len(list))
	response := rand.Intn(len(DecideResponses))
	chosen := strings.Trim(list[num], " ;")
	ret := fmt.Sprintf(DecideResponses[response], chosen)

	if strings.Count(strings.Join(args[1:], " "), ",") > 0 &&  strings.Count(strings.Join(args[1:], " "), ";") > 0 {
		ret += "\n\n(btw, if you use both ; and , as a delimiter, I'm only going to pay attention to the semicolons)"
	}

	logDebug("Decide list (decided on number %d): %s\n", num, list)
	bot.sendMsg(msg.ChannelID, "%s", ret)
}

func (bot *SumomoInstance) cmdPoll(msg *discordgo.MessageCreate, args []string) {
	logInfo("Creating poll, args: %s\n", args)

	if last, found := lastPoll[msg.GuildID]; found {
		if time.Now().Sub(time.Unix(last, 0)).Seconds() < POLL_COOLDOWN_LIMIT {
			bot.sendMsg(msg.ChannelID, "Sorry, polls are limited to one every %v seconds, wait %v more second",
				POLL_COOLDOWN_LIMIT,
				POLL_COOLDOWN_LIMIT - int64(time.Now().Sub(time.Unix(last, 0)).Seconds()))
			return
		}
	}

	if bot.maybeSendHelp(msg, args, fmt.Sprintf(`Usage: **!poll** \"[your question here]\" [options]

The question **MUST** be in quotes.  Options must be separated by commas or semicolons, and you can have up to ten of them.

For example:
%s`, "```\n!poll \"Who is the best maid?\" Rosy from The Jetsons, your mom, Sakua Izayoi\n```")) {
	    return
    }

	switch len(args) {
	case 1:
		bot.sendMsg(msg.ChannelID, "No question asked!  Silly...")
		return

	case 2:
		bot.sendMsg(msg.ChannelID, "A poll with no options?  How does that work?　(・・。)ゞ")
		return
	}

	// Combine the rest of the args into one string so we can split
	// them up correctly.
	question := args[1]
	optStr := strings.Join(args[2:], " ")
	optStr = strings.ReplaceAll(optStr, ",", ";")
	optionsWithDups := strings.Split(optStr, ";")

	optionsCheck := make(map[string]bool, 0)
	options := make([]string, 0)

	for _, opt := range(optionsWithDups) {
		if _, found := optionsCheck[strings.TrimSpace(opt)]; !found {
			optionsCheck[strings.TrimSpace(opt)] = true
			options = append(options, strings.TrimSpace(opt))
		}
	}

	if len(options) == 1 {
		bot.sendMsg(msg.ChannelID, "A poll with one option?  That sounds fishy...")
		return
	}

	lastPoll[msg.GuildID] = time.Now().Unix()

	logInfo("Poll: %q\n  Poll questions: %v\n", question, options)

	var build strings.Builder
	user, err := bot.Instance.GuildMember(msg.GuildID, msg.Author.ID)

	if err == nil {
		build.WriteString(fmt.Sprintf("** == POLL TIME! == **\n%s asks... %s\n\n**Your Options:**\n",
			user.Nick, question))
	} else {
		build.WriteString(fmt.Sprintf("** == POLL TIME! == **\n%s asks... %s\n\n**Your Options:**\n",
			msg.Author.Username, question))
	}

	emojiOpts := make([]*emojiMapping, 0)
	for idx, opt := range(options) {
		switch idx {
		case 0: emojiOpts = append(emojiOpts, &emojiMapping{":zero:", "0️⃣"})
		case 1: emojiOpts = append(emojiOpts, &emojiMapping{":one:", "1️⃣"})
		case 2: emojiOpts = append(emojiOpts, &emojiMapping{":two:", "2️⃣"})
		case 3: emojiOpts = append(emojiOpts, &emojiMapping{":three:", "3️⃣"})
		case 4: emojiOpts = append(emojiOpts, &emojiMapping{":four:", "4️⃣"})
		case 5: emojiOpts = append(emojiOpts, &emojiMapping{":five:", "5️⃣"})
		case 6: emojiOpts = append(emojiOpts, &emojiMapping{":six:", "6️⃣"})
		case 7: emojiOpts = append(emojiOpts, &emojiMapping{":seven:", "7️⃣"})
		case 8: emojiOpts = append(emojiOpts, &emojiMapping{":eight:", "8️⃣"})
		case 9: emojiOpts = append(emojiOpts, &emojiMapping{":nine:", "9️⃣"})
		default:
			bot.sendMsg(msg.ChannelID, "Sorry, too many poll options! >_<")
			return
		}

		build.WriteString(fmt.Sprintf("%s %s\n", emojiOpts[len(emojiOpts) - 1].emoji, strings.TrimSpace(opt)))
	}

	poll, err := bot.Instance.ChannelMessageSend(msg.ChannelID, build.String())
	if err == nil {
		for _, emoji := range(emojiOpts) {
			bot.Instance.MessageReactionAdd(msg.ChannelID, poll.ID, emoji.raw)
		}
	} else {
		bot.sendMainAdminDM("Uh oh, I couldn't create a poll message!\n```\n%v\n```", err)
		bot.sendMsg(msg.ChannelID, "Uh oh, couldn't create the poll message!  I've let the main admin know.")
	}
}

func (bot *SumomoInstance) cmdBigAvatar(msg *discordgo.MessageCreate, args []string) {
	logInfo("Enlarging avatar, args: %s\n", args)

	if bot.maybeSendHelp(msg, args, `Usage: **!bigavatar** _<@username or user ID>_`) {
	    return
    }

	if len(args) != 2 {
		bot.sendMsg(msg.ChannelID, "Please supply one username or ID at a time")
	}

	var user *discordgo.User
	var avatar image.Image
	var err error

	if user, err = bot.Instance.User(args[1]); err != nil {
		bot.sendMsg(msg.ChannelID, "Can't find that user")
		return
	}

	if avatar, err = bot.Instance.UserAvatarDecode(user); err != nil {
		logError("Cannot get avatar for user %s: %v", args[1], err)
		bot.sendMsg(msg.ChannelID, "Can't get that user's avatar")
		return
	}

	bigAvatar := imaging.Resize(avatar, 800, 0, imaging.Lanczos)

	var imageIO bytes.Buffer
	enc := &png.Encoder{ CompressionLevel: png.BestCompression }
	enc.Encode(&imageIO, bigAvatar)

	filename := fmt.Sprintf("user-avatar-%s.png", user.ID)
	imgReader := bytes.NewReader(imageIO.Bytes())
	embed := &discordgo.MessageSend{
		Embed: &discordgo.MessageEmbed{
			Image: &discordgo.MessageEmbedImage{
				URL: "attachment://" + filename}},

		Files: []*discordgo.File{
			&discordgo.File{
				Name:   filename,
				Reader: imgReader}}}

	bot.Instance.ChannelMessageSendComplex(msg.ChannelID, embed)
}

/*==============================================================================
 * Admin-Only Bot Commands
 *============================================================================*/

func (bot *SumomoInstance) cmdAdminChangeNowPlaying(msg *discordgo.MessageCreate, args []string) {
	bot.NowPlayingHandler(true)
}

//
// Implements the admin-only command !displayconfig
//
// This must be run in the admin control channel.
//
func (bot *SumomoInstance) cmdAdminDisplayConfig(msg *discordgo.MessageCreate, args []string) {
	if bot.ensureChannel(msg, bot.Conf.Permissions.AdminControlChannel, "!displayconfig") {
		bot.sendMsg(msg.ChannelID, "```\n%s\n```", strings.TrimSpace(bot.Conf.generateConfigReport()))
	}
}

func (bot *SumomoInstance) cmdAdminReloadConfig(msg *discordgo.MessageCreate, args []string) {
	if bot.ensureChannel(msg, bot.Conf.Permissions.AdminControlChannel, "!reloadconfig") {
		newCfg, err := parseConfigFile(bot.ConfigFilename)

		if err != nil {
			bot.sendMsg(msg.ChannelID, "*ERROR:** Could not reload configuration, keeping old one")
		}

		report, ok := newCfg.checkConfigWithReport()
		if ok {
			if bot.Perms.populate(bot, newCfg) {
				bot.sendMsg(msg.ChannelID, "New configuration loaded successfully")
				time.Sleep(500 * time.Millisecond)
				bot.sendMsg(msg.ChannelID, "%s", report)

				bot.Conf = newCfg
				time.Sleep(500 * time.Millisecond)
				bot.sendMsg(msg.ChannelID, "New configuration is now in effect")
			} else {
				bot.sendMsg(msg.ChannelID, "New configuration has permission errors, keeping old config and permission")
			}
		} else {
			bot.sendMsg(msg.ChannelID, "New configuration is not valid, keeping old one")
		}
	}
}

func (bot *SumomoInstance) cmdAdminReloadLuaScripts(msg *discordgo.MessageCreate, args []string) {
	if bot.ensureChannel(msg, bot.Conf.Permissions.AdminControlChannel, "!reloadluascripts") {
		newTable := make(LuaCommandTable, 0)
		success := true

		mainErr := filepath.Walk(bot.Conf.Global.LuaDirectory, func(filename string, fInfo os.FileInfo, fError error) error {
			cmdName := filepath.Base(filename)
			ext := filepath.Ext(filename)

			cmdName = "!" + cmdName[:strings.LastIndex(cmdName, ext)]

			if !fInfo.IsDir() {
				if proto, err := loadLuaFile(filename); err != nil {
					bot.sendMsg(msg.ChannelID, "**Error loading file %s**: %s", filename, err)
					success = false
				} else if err = newTable.registerLuaCommand(cmdName, proto); err != nil {
					bot.sendMsg(msg.ChannelID, "**Error registering file %s**: %s", filename, err)
					success = false
				}
			}

			return nil
		})

		if mainErr != nil {
			bot.sendMsg(msg.ChannelID, "**Error walking directory %s**: %s", bot.Conf.Global.LuaDirectory, mainErr)
			success = false
		}

		if !success {
			bot.sendMsg(msg.ChannelID, "Errors encountered while loading Lua scripts, keeping old Lua command table")
		} else {
			luaCommands = newTable
			bot.sendMsg(msg.ChannelID, "Success, %d commands in Lua command table", len(luaCommands))
		}
	}
}
