// Sumomo-Go
// Copyright(C) 2018-2019 Alexa Jones-Gonzales
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/.>
package main

//
// Lua Scripting Core
//

import (
	"bufio"
	"fmt"
	"os"

	"github.com/bwmarrin/discordgo"
	"github.com/yuin/gopher-lua"
	luaparse "github.com/yuin/gopher-lua/parse"
)

type luaCommand struct {
	name        string
	description string
	needsAdmin  BuiltInAdminPerm
	proto       *lua.FunctionProto
}

type LuaCommandTable map[string]*luaCommand

var luaCommands = make(LuaCommandTable, 0)

//
// Gets a new Lua state that is set up for use here in Sumomo
//
func getLuaState() (*lua.LState, error) {
	ret := lua.NewState(lua.Options{SkipOpenLibs: true})
	pkgs := []struct{
		n string
		f lua.LGFunction
	}{
		{lua.LoadLibName, lua.OpenPackage},
		{lua.BaseLibName, lua.OpenBase},
		{lua.TabLibName, lua.OpenTable},
		{lua.StringLibName, lua.OpenString}}

	for _, fnPair := range(pkgs) {
		p := lua.P{
			Fn: ret.NewFunction(fnPair.f),
			NRet: 0,
			Protect: true}

		if err := ret.CallByParam(p, lua.LString(fnPair.n)); err != nil {
			return nil, err
		}
	}

	ret.PreloadModule("sumomo", initLuaSumomoModule)

	return ret, nil
}

//
// Loads a Lua script from the given file
//

func loadLuaFile(filename string) (*lua.FunctionProto, error) {
	logInfo("Loading Lua file: %q\n", filename)
	inFile, err := os.Open(filename)
	defer inFile.Close()

	if err != nil {
		return nil, err
	}

	reader := bufio.NewReader(inFile)
    chunk, err := luaparse.Parse(reader, filename)
    if err != nil {
        return nil, err
    }

    ret, err := lua.Compile(chunk, filename)
    if err != nil {
        return nil, err
    }

	logDebug("Loaded and compiled Lua file successfully: %q\n", filename)
	return ret, nil
}

const (
	LUA_FN_SUMOMO_DESCRIPTION = "sumomoDescription"
	LUA_FN_SUMOMO_PERMS       = "sumomoPerms"
	LUA_FN_SUMOMO_COMMAND     = "sumomoCommand"
)

//
// Calls the function "sumomoDescription()" in the given Lua state.
// Returns the description for the command and nil on success, or an
// empty string and an error otherwise.
//
func luaGetDescription(state *lua.LState) (string, error) {
	fn := state.GetGlobal(LUA_FN_SUMOMO_DESCRIPTION)

	if fn == nil {
		return "", fmt.Errorf("Could not find description function: %s\n", LUA_FN_SUMOMO_DESCRIPTION)
	}

	err := state.CallByParam(lua.P{
		Fn: fn,
		NRet: 1,
		Protect: false}, nil)

	if err != nil {
		return "", err
	}

	ret := state.Get(-1)
	state.Pop(1)

	return ret.String(), nil
}

//
// Calls the function "sumomoPerms()" in the given Lua state.
// Returns the admin permissions for the command and nil on success,
// or -1 and an error otherwise.
//
func luaGetPerms(state *lua.LState) (BuiltInAdminPerm, error) {
	fn := state.GetGlobal(LUA_FN_SUMOMO_PERMS)

	if fn == nil {
		return -1, fmt.Errorf("Could not find permissions function: %s\n", LUA_FN_SUMOMO_PERMS)
	}

	err := state.CallByParam(lua.P{
		Fn: fn,
		NRet: 1,
		Protect: false})

	if err != nil {
		return 0, err
	}

	ret := state.Get(-1)
	state.Pop(1)

	val := ret.String()
	switch val {
	case "anyAdmin":
		return AdminPerm_AnyAdmin, nil

	case "mainAdmin":
		return AdminPerm_MainAdmin, nil

	case "none":
		return AdminPerm_NotRequired, nil

	default:
		return -1, fmt.Errorf("%s returned an invalid value: %s\n", LUA_FN_SUMOMO_PERMS, ret.String())

	}
}

//
// Registers a command from a Lua script
//
func (tbl LuaCommandTable) registerLuaCommand(name string, proto *lua.FunctionProto) error {
	if _, found := tbl[name]; found {
		return fmt.Errorf("Attempted to register a duplicate Lua command: %v\n", name)
	}

	state, err := getLuaState()
	defer state.Close()

	if err != nil {
		return err
	}

	luaFn := state.NewFunctionFromProto(proto)
	state.Push(luaFn)
	if err := state.PCall(0, lua.MultRet, nil); err != nil {
		return err
	}

	desc, err := luaGetDescription(state)
	if err != nil {
		return err
	}

	perm, err := luaGetPerms(state)
	if err != nil {
		return err
	}

	newCmd := &luaCommand{
		name,
		desc,
		perm,
		proto}

	tbl[name] = newCmd

	logInfo("Registered new Lua command: %q\n", name)
	return nil
}

//
// Searches for a Lua command by name.  Returns the *luaCommand struct
// associated with it if it was found, or nil otherwise.  This will
// not prepend the exclamation point on the command name, so you are
// expected to pass a string with that already added.
//
func (tbl LuaCommandTable) findLuaCommand(cmd string) *luaCommand {
	if cmdObj, found := tbl[cmd]; found {
		return cmdObj
	} else {
		return nil
	}
}

//
// Calls a Lua command
//
func (bot *SumomoInstance) callLuaCommand(cmd *luaCommand, luaState *lua.LState, msg *discordgo.MessageCreate, args []string) error {
	logInfo("Calling Lua command %q, args: %v\n", cmd.name, args)
	luaFn := luaState.NewFunctionFromProto(cmd.proto)
	luaState.Push(luaFn)
	if err := luaState.PCall(0, lua.MultRet, nil); err != nil {
		return err
	}

	p := lua.P{
		Fn: luaState.GetGlobal(LUA_FN_SUMOMO_COMMAND),
		NRet: 1,
		Protect: true}

	argTbl := luaState.NewTable()
	for _, str := range(args) {
		argTbl.Append(lua.LString(str))
	}

	err := luaState.CallByParam(p,
		lua.LString(msg.GuildID),
		lua.LString(msg.ChannelID),
		lua.LString(msg.Author.ID),
		argTbl)

	if err != nil {
		bot.sendMsg(msg.ChannelID, "Internal command error, the admin has been notified")
		return err
	}

	ret := luaState.Get(-1)
	luaState.Pop(1)

	bot.sendMsg(msg.ChannelID, ret.String())
	return nil
}
