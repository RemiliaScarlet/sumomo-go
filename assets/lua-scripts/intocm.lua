local sumomo = require("sumomo")

function sumomoDescription()
   return "Converts Inches to Centimeters."
end

function sumomoPerms()
   return "none"
end

function sumomoCommand(server, channel, author, args)
   local numArgs = sumomo.TableLen(args)

   if numArgs == 2 then
      if args[2] == "--help" or args[2] == "-h" then
         return "Usage: **!intocm** _<number>_\n"
      end
   end

   inches = tonumber(args[2])
   if inches == nil then
      return "I can't understand that number ┐(￣ヘ￣;)┌"
   end

   cm = inches * 2.54

   ret = string.format("%.2f° inches = %.2f° centimeters", inches, cm)

   if numArgs > 2 then
      ret = ret .. "\n\nbtw... I can only look at one number at a time (^_~)"
   end

   return ret
end
