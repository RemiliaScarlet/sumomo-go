package main

import (
	"strings"
	"testing"
)

func TestConfigParse(t *testing.T) {
	in := strings.NewReader(fakeGoodConfig)
	conf, err := parseConfigStream(in)

	if err != nil {
		t.Errorf("Config parsing error: %s\n", err)
	}

	if conf == nil {
		t.Errorf("Config parsing error, conf was nil\n")
	}
}

func TestMalformedConfigParse(t *testing.T) {
	malIn := strings.NewReader(malformedConfig)
	malConf, malErr := parseConfigStream(malIn)

	if malErr == nil {
		t.Errorf("Malformed config should have produced an error\n")
	}

	if malConf != nil {
		t.Errorf("Malformed config should have produced a nil config\n")
	}
}

const fakeGoodConfig = `
{
    global: {
        token: "FAKE TOKEN HERE"
        loglevel: error
        nowplaying: [ "Planning world domination", "with playdough", "with fire", "Clarinet Hero", "Immortal Combat 2", "M-x Butterfly", "doctor" ]
        nowplayingtime: 20
    }

    permissions: {
        commands: {
            tof: [ "allow" ]
            toc: [ "allow" ]
            define: [ "allow" ]
            spell: [ "allow" ]
            weather: [ "allow" ]
            decide: [ "allow" ]
            remind: []
            bigmoji: [ "allow" ]
            ping: [ "all-admins" ]
            dice: [ "allow" ]
            8ball: [ "allow" ]
            help: [ "allow" ]
            fortune: [ "allow" ]
        }

        admin: "1234567890"
        otheradmins: ["9876543210"]
        admincontrolchannel: "4236"
    }

    loudness: {
        loudnessminlen: 3
        loudnessthreshold: 50
        notloudwords: ["lol", "roflmao", "rofl", "lmao", "what"]
        loudnesswaittime: 60
    }

    images: {
        filter: nearest
    }

    reminders: {
        database: "/tmp/sumomo-reminders.db"
        checktime: 60
    }

    weather: {
        key: ""
        bothunits: true
        includeforecasts: false
        numforecastdays: 3
    }

    bigmoji: {
        cachelimit: 20
        resize: 192
    }
}
`

const fakeBadConfig = `
{
    global: {
        token: ""
        loglevel: foo
        nowplaying: [ "Planning world domination", "with playdough", "with fire", "Clarinet Hero", "Immortal Combat 2", "M-x Butterfly", "doctor" ]
        nowplayingtime: -20
    }

    permissions: {
        commands: {
            tof: [ "allow" ]
            toc: [ "allow" ]
            define: [ "allow" ]
            spell: [ "allow" ]
            weather: [ "allow" ]
            decide: [ "allow" ]
            remind: []
            bigmoji: [ "allow" ]
            ping: [ "all-admins" ]
            dice: [ "allow" ]
            8ball: [ "allow" ]
            help: [ "allow" ]
            fortune: [ "allow" ]
        }

        admin: ""
        otheradmins: []
        admincontrolchannel: ""
    }

    loudness: {
        loudnessminlen: 3
        loudnessthreshold: 50
        notloudwords: ["lol", "roflmao", "rofl", "lmao", "what"]
        loudnesswaittime: 60
    }

    images: {
        filter: blah
    }

    reminders: {
        database: "/tmp/sumomo-reminders.db"
        checktime: 60
    }

    weather: {
        key: ""
        bothunits: true
        includeforecasts: false
        numforecastdays: 3
    }

    bigmoji: {
        cachelimit: 20
        resize: 192
    }
}
`

const malformedConfig = `
{ blah
`
