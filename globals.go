// Sumomo-Go
// Copyright(C) 2018-2019 Alexa Jones-Gonzales
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/.>
package main

//
// Constants and Global Variables
//

import (
	"errors"
	"math"
)

const (
	programName = "Sumomo-Go"
	programVersion = "0.3.1"
	DictUrl = "https://googledictionaryapi.eu-gb.mybluemix.net/?"
	deg2Rad = math.Pi / 180
)

type emojiMapping struct {
	emoji string
	raw string
}

const POLL_COOLDOWN_LIMIT = 30
var lastPoll = make(map[string]int64, 0)

var (
	notAnimatedErr = errors.New("Not animated")
	isAnimatedErr = errors.New("Animated")
)

var CapsLockMessages = [...]string{"https://i.imgur.com/fVHcQ3r.jpg",
	"https://i.imgur.com/VXLrC4w.jpg",
	"https://i.imgur.com/m1GshcJ.jpg",
	"https://i.imgur.com/tNfxf7V.jpg",
	"https://i.imgur.com/ThHht7l.jpg",
	"https://www.youtube.com/watch?v=yQzAUAe6mjM",
	"https://i.imgur.com/nD4MPS7.gif"}

var EightBallAnswers = [...]string{"It is certain.",
    "It is decidedly so.",
    "Without a doubt.",
    "Yes - definitely.",
    "You may rely on it.",
    "As I see it, yes.",
    "Most likely.",
    "Outlook good.",
    "Yes.",
    "Signs point to yes.",

    "Reply is hazy, try again.",
    "Ask again later.",
    "Better not tell you now.",
    "Cannot predict right now.",
    "Concentrate and ask again.",

    "Don't count on it.",
    "My reply is \"no\".",
    "My sources say no.",
    "The outlook is not so good.",
    "Very doubtful."}

var DecideResponses = [...]string{"How about _%s_?",
    "Hmm, how about _%s_?　(・・。)ゞ",
    "That's a hard one.  What if you went with _%s_?　(⑅ ‘﹃’ )",
    "I think I would choose _%s_.",
    "Try _%s_　⊂( ・ ̫・)⊃",
    "Heck I dunno, what about _%s_?　╮(︶▽︶)╭",
    "Does _%s_ sound OK?",
    "Definitely go with _%s_　(　^ω^）",
	"My gut tells me that _%s_ is the best choice."}

var WeatherIcons = map[string]string{
	"01d":":sunny:",
	"02d":":white_sun_small_cloud:",
	"03d":":white_sun_cloud:",
	"04d":":cloud:",
	"09d":":white_sun_rain_cloud:",
	"10d":":cloud_rain:",
	"11d":":thunder_cloud_rain:",
	"13d":":snowflake:",
	"50d":":foggy:",

	"01n":":crescent_moon:",
	"02n":":white_sun_small_cloud:",
	"03n":":white_sun_cloud:",
	"04n":":cloud:",
	"09n":":white_sun_rain_cloud:",
	"10n":":cloud_rain:",
	"11n":":thunder_cloud_rain:",
	"13n":":snowflake:",
	"50n":":foggy:"}
