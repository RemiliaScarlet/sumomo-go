// Sumomo-Go
// Copyright(C) 2018-2019 Alexa Jones-Gonzales
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/.>
package main

//
// !remind command
//

import (
	"database/sql"
	"fmt"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"


	_ "github.com/mattn/go-sqlite3"
	"github.com/bwmarrin/discordgo"
	"scarletdevilmansion.tech/go/argparser-go"
)

/*==============================================================================
 * Structs and Consts
 *============================================================================*/

const ReminderSubsystemVersion = "1.1"
const RemindersMaxListLen = 69

type Reminder struct {
	Id   int64
	User int64      // The ID of the user
	When time.Time  // When to remind the user
	Message string  // What to remind them about
}

type ReminderSubsystem struct {
	parser *argparser.ArgParser
	DBConn *sql.DB
	ReminderFile string
	Reminders map[int64]*Reminder
	In chan *Reminder
	wasInit bool
	bot *SumomoInstance
}

func NewReminderSubystem() *ReminderSubsystem {
	ret := &ReminderSubsystem{
		nil,
		nil,
		"",
		make(map[int64]*Reminder, 0),
		make(chan *Reminder, 0),
		false,
		nil,
	}

	return ret
}

func (sys *ReminderSubsystem) Init(bot *SumomoInstance) error {
	sys.wasInit = false
	sys.bot = bot
	sys.setupArgParser(sys.bot)

	logInfo("Initializing Reminder system\n")

	sys.ReminderFile = strings.TrimSpace(sys.bot.Conf.Reminders.Database)
	if sys.ReminderFile == "" {
		logWarning("No reminder database set, not initializing subsystem\n")
		return nil
	}

	if err := sys.openDB(); err != nil {
		return err
	}

	if err := sys.checkDBTables(); err != nil {
		return err
	}

	sys.wasInit = true
	return nil
}

func (system *ReminderSubsystem) cmdRemindHelp(channel string) {
	var ret strings.Builder

	ret.WriteString("```\nUsage: !remind [options]")

	longestLongArg := system.parser.GetLongestArgName()

	ret.WriteString(system.parser.PreHelpText)
	ret.WriteRune('\n')

	for _, group := range system.parser.GetArgumentGroups() {
		// Write the group header
		if group.Name == "" {
			ret.WriteString("\nGeneral Options\n")
		} else {
			ret.WriteString(fmt.Sprintf("\n%s\n", group.Name))
		}

		ret.WriteString("=========\n")

		for _, arg := range group.Args {
			// Write the long name
			ret.WriteString(fmt.Sprintf("--%s", arg.Name))

			if arg.Type == argparser.AT_Flag || arg.Type == argparser.AT_MultiFlag {
				ret.WriteString("  ")
			} else {
				ret.WriteString(" x")
			}

			argparser.WriteIndent(&ret, longestLongArg - len(arg.Name) + 1)

			if arg.ShortName != 0 {
				ret.WriteString(fmt.Sprintf(" / -%c ", arg.ShortName))

				if arg.Type == argparser.AT_Flag || arg.Type == argparser.AT_MultiFlag {
					ret.WriteString("  : ")
				} else {
					ret.WriteString("x : ")
				}
			} else {
				ret.WriteString("        : ")
			}

			argparser.PrintArgumentDescription(&ret, arg.HelpString, longestLongArg + 14)
		}
	}

	ret.WriteString("\n```")
	ret.WriteRune('\n')
	ret.WriteString(system.parser.PostHelpText)
	ret.WriteString("\n")

	system.bot.sendMsg(channel, "%s", ret.String())
}

func (system *ReminderSubsystem) setupArgParser(bot *SumomoInstance) {
 	ret := argparser.NewArgParserExtended("!remind", ReminderSubsystemVersion, "help", '\000', "version", '\000', nil, nil, false, false)

	ret.PostHelpText = fmt.Sprintf("You must use either the `--time/-t` argument, or the `--in/-i` argument, but not both.  When using the `--time/-t` argument, any string that follows RFC 3339 can be specified (see <https://en.wikipedia.org/wiki/ISO_8601> or ping **%s** for details).  Be sure to enclose this in double-quotes, however.  Alternatively, `--in/-i` might be easier since it doesn't rely on a timezone, but you might have to do some mental math in your head.\n\n",
		bot.getUsernameByID(bot.Conf.Permissions.Admin))

	ret.PostHelpText += "Anything that is not an argument is treated as part of the reminder message.\n\n"
	ret.PostHelpText += "Example usage: `!remind --time 2019-12-05T13:00:00Z-0700 Happy birthday, Remilia!`"

	ret.AddArgument("time", 't', argparser.AT_String, nil, "",
		fmt.Sprintf("Specify a specific time and date for the reminder.  This cannot be used with --in/-i.  If you do not specify a timezone in this string, the timezone is assumed to be the same as Sumomo's (%s).",
			bot.Conf.Global.Timezone))

	ret.AddArgument("in", 'i', argparser.AT_String, "", "",
		"Specify how far in the future you want the reminder.  If you just specify a number, then this will default to the number of minutes into the future you want the reminder.  Alternatively, append 'seconds', 'minutes', 'hours', 'days', or 'months' onto the number, such as '42hours' for 42 hours in the future.  This is case insensitive.  The offset must be greater than 0 and must be an integer.")

	ret.AddArgument("list", 'l', argparser.AT_Flag, nil, "", "List out all of your current reminders.")
	ret.AddArgument("delete", 'd', argparser.AT_Int, int64(-1), "", "Delete the specified reminder from your list of reminders. Check --list/-l to get the ID to pass here.")

	// We're managing our own --help
	ret.AddArgument("help", 'h', argparser.AT_Flag, false, "", "Shows this help message")

	system.parser = ret
}

/*==============================================================================
 * DB Initialization
 *============================================================================*/

func (system *ReminderSubsystem) openDB() error {
	logInfo("Opening (or creating) reminder database: %s\n", system.ReminderFile)

	var err error
	filename := url.PathEscape(system.ReminderFile)

	system.DBConn, err = sql.Open("sqlite3", fmt.Sprintf("file:%s?mode=rwc", filename))
	if err != nil {
		logError("Failed to open (or create) reminder database\n")
		return err
	}

	return nil
}

func (system *ReminderSubsystem) checkDBTables() error {
	logInfo("Checking for required tables in reminder database\n")

	tblQuery := "select name from main.sqlite_master where type='table';"

	if tables, err := system.DBConn.Query(tblQuery); err != nil {
		return err
	} else {
		foundReminders := false

		for tables.Next() {
			var name string

			if err = tables.Scan(&name); err != nil {
				return err
			}

			if name == "REMINDERS" {
				foundReminders = true
			}
		}

		if !foundReminders {
			logInfo("Creating REMINDERS table\n")
			if err = system.createRemindersTable(); err != nil {
				logError("Could not create REMINDERS table; %s\n", err)
				return err
			}
		} else {
			logInfo("REMINDERS table found\n")
		}
	}

	return nil
}

func (system *ReminderSubsystem) createRemindersTable() error {
	create := `
create table REMINDERS
    (ID integer unique not null primary key autoincrement,
     USER integer not null,
     TIME integer not null,
     DESC text);
`

	_, err := system.DBConn.Exec(create)
	return err
}

/*==============================================================================
 * DB Queries and Check Loop
 *============================================================================*/

func (bot *SumomoInstance) ReminderLoop(system *ReminderSubsystem) {
	if !system.wasInit {
		return
	}

	logInfo("Starting Reminder loop\n")

	var newRem *Reminder
	cycles := 0

	for {
		time.Sleep(1 * time.Second)

		select {
		case newRem = <-system.In:
			cycles++

			logInfo("Received new reminder in Reminder loop\n")
			if err := system.addReminder(newRem); err != nil {
				logError("Error inserting new reminder in database: %s\n", err)
				logError("** Ending check loop for reminder system **\n")

				bot.sendMainAdminDM("Error inserting new reminder in database: %s", err)
				bot.sendMainAdminDM("Ended check loop for reminder system")

				return
			}

		default:
			cycles++

			if cycles >= bot.Conf.Reminders.CheckTime {
				if err := system.updateInstances(); err != nil {
					logError("Error updating reminder instances: %s\n", err)
					logError("** Ending check loop for reminder system **\n")

					bot.sendMainAdminDM("Error updating reminder instances: %s", err)
					bot.sendMainAdminDM("Ended check loop for reminder system")

					return
				}

				system.sendReminders(bot)

				cycles = 0
			}
		}
	}
}

//
// Inserts a new reminder into the database.  This does not actually
// update the map in system.
//
func (system *ReminderSubsystem) addReminder(rem *Reminder) error {
	tx, err := system.DBConn.Begin()
	if err != nil {
		return err
	}

	statement, err := tx.Prepare("insert into REMINDERS(user, time, desc) values(?, ?, ?)")
	if err != nil {
		return err
	}
	defer statement.Close()

	_, err = statement.Exec(rem.User, strconv.FormatInt(rem.When.UnixNano(), 10), rem.Message)
	if err != nil {
		return err
	}

	if err = tx.Commit(); err != nil {
		return err
	} else {
		logDebug("Successfully inserted reminder into database for user %d\n", rem.User)
	}

	logInfo("Added new reminder for %d, scheduled for %q: %s\n", rem.User, rem.When.String(), rem.Message)
	return nil
}

//
// Updates the map in system to reflect the state of the database
//
func (system *ReminderSubsystem) updateInstances() error {
	queryStr := "select * from REMINDERS;"
	rems, err := system.DBConn.Query(queryStr)

	if err != nil {
		return err
	}

	for rems.Next() {
		var id, user, rawWhen int64
		var desc string

		if err = rems.Scan(&id, &user, &rawWhen, &desc); err != nil {
			return err
		}

		when := time.Unix(0, rawWhen)
		if oldRem, found := system.Reminders[id]; found {
			// See if this old reminder is the same
			if oldRem.Message != desc {
				logWarning("Reminder with a duplicate ID (%d) found in memory, updating message: %q => %q\n",
					id, oldRem.Message, desc)
				oldRem.Message = desc
			}

			if oldRem.User != user {
				logWarning("Reminder with a duplicate ID (%d) found in memory, updating userID: %v => %v\n",
					id, oldRem.User, user)
				oldRem.User = user
			}

			if oldRem.When != when {
				logWarning("Reminder with a duplicate ID (%d) found in memory, updating time: %q => %q\n",
					id, oldRem.When.String(), when.String())
				oldRem.When = when
			}

		} else if !found {
			system.Reminders[id] = &Reminder{
				id,
				user,
				when,
				desc}
		}
	}

	return nil
}

func (system *ReminderSubsystem) sendReminders(bot *SumomoInstance) {
	//logDebug("Doing sendReminders()\n")
	toRemove := make([]int64, 0, 0)

	for id, rem := range(system.Reminders) {
		if rem.When.Before(time.Now()) {
			logInfo("Sending reminder to %v\n", rem.User)
			bot.sendDM(strconv.FormatInt(rem.User, 10), fmt.Sprintf("**A reminder from Sumomo!**\n\n%s", rem.Message))
			toRemove = append(toRemove, id)
		}
	}

	for _, remId := range(toRemove) {
		tx, err := system.DBConn.Begin()
		if err != nil {
			panic(err)
		}

		statement, err := tx.Prepare("delete from REMINDERS where ID=?")
		if err != nil {
			panic(err)
		}
		defer statement.Close()

		_, err = statement.Exec(remId)
		if err != nil {
			panic(err)
		}

		if err = tx.Commit(); err != nil {
			panic(err)
		} else {
			logDebug("Removed remider with id %d for user %d from database\n", remId, system.Reminders[remId].User)
		}

		delete(system.Reminders, remId)
	}
}


func (system *ReminderSubsystem) getUserReminders(user string) ([]*Reminder, error) {
	ret := make([]*Reminder, 0, 0)

	queryStr := "select * from REMINDERS where USER=?;"
	rems, err := system.DBConn.Query(queryStr, user)

	if err != nil {
		return ret, err
	}

	for rems.Next() {
		var id, userID, rawWhen int64
		var desc string

		if err = rems.Scan(&id, &userID, &rawWhen, &desc); err != nil {
			return ret, err
		}

		when := time.Unix(0, rawWhen)
		ret = append(ret, &Reminder{id, userID, when, desc})
	}

	return ret, nil
}

/*==============================================================================
 * !reminder command implementation
 *============================================================================*/

func getReminderTimeStr(str string) (time.Time, error) {
	return time.Parse(time.RFC3339, str)
}

func getReminderTimeOffset(str string) (time.Time, error) {
	reg := regexp.MustCompile(`^([0-9]+)([a-zA-Z]+)??$`)
	matches := reg.FindAllStringSubmatch(str, -1)

	if len(matches) != 1 {
		logInfo("Bad number of matches in %q: %d\n", str, len(matches))
		return time.Now(), fmt.Errorf("Invalid offset string: %q", str)
	}

	if len(matches[0]) != 3 {
		logInfo("Bad number of submatches in %q: %d\n", str, len(matches[0]))
		return time.Now(), fmt.Errorf("Invalid offset string: %q", str)
	}

	if matches[0][1] == "" {
		return time.Now(), fmt.Errorf("No number specified in offset")
	}

	units := "minutes"
	num, err := strconv.ParseInt(matches[0][1], 10, 64)

	if err != nil {
		return time.Now(), fmt.Errorf("Invalid number specified in offset")
	}

	if num <= 0 {
		return time.Now(), fmt.Errorf("Sorry, the time offset must be greater than 0")
	}

	if matches[0][2] != "" {
		units = matches[0][2]
	}

	if units[len(units) - 1] == 's' {
		units = units[:(len(units) - 1)]
	}

	switch strings.ToLower(units) {
	case "second":
		return time.Now().Add(time.Duration(num) * time.Second), nil

	case "minute":
		return time.Now().Add(time.Duration(num) * time.Minute), nil

	case "hour":
		return time.Now().Add(time.Duration(num) * time.Hour), nil

	case "day":
		return time.Now().Add(time.Duration(num) * time.Hour * 24), nil

	case "month":
		return time.Now().Add(time.Duration(num) * time.Hour * 730), nil  // Approximate

	default:
		return time.Now(), fmt.Errorf("Sorry, invalid offset unit: %s", units)
	}
}

func (system *ReminderSubsystem) deleteReminder(id int64, user string) error {
	rems, err := system.getUserReminders(user)

	if err != nil {
		return err
	}

	found := false
	for _, rem := range(rems) {
		if rem.Id == id {
			found = true
			break
		}
	}

	if !found {
		return fmt.Errorf("Sorry, no reminder with the ID of %d was found for you.  It either doesn't exist, or is owned by another user.", id)
	}

	tx, err := system.DBConn.Begin()
	if err != nil {
		panic(err)
	}

	statement, err := tx.Prepare("delete from REMINDERS where ID=?")
	if err != nil {
		panic(err)
	}
	defer statement.Close()

	_, err = statement.Exec(id)
	if err != nil {
		panic(err)
	}

	if err = tx.Commit(); err != nil {
		panic(err)
	} else {
		logDebug("Removed remider with id %d for user %d from database at user's request\n", id, system.Reminders[id].User)
	}

	delete(system.Reminders, id)

	return nil
}

//
// Implements the !remind command
//
func (bot *SumomoInstance) cmdRemind(msg *discordgo.MessageCreate, rawArgs []string) {
	// Always reset the parser after we exit this function
	defer bot.ReminderSys.parser.Reset()

	logDebug("cmdRemind called: %q\n", rawArgs)

	if err := bot.ReminderSys.parser.Parse(rawArgs, false, false); err != nil {
		bot.sendMsg(msg.ChannelID, "%s\n", err)
		return
    }

	if bot.ReminderSys.parser.ArgCalledP("--help") || len(rawArgs) == 1 {
		bot.ReminderSys.cmdRemindHelp(msg.ChannelID)
		return
	}

	posArgs := ""
	for _, str := range(bot.ReminderSys.parser.PositionalArgs) {
		posArgs += str
		posArgs += " "
	}
	posArgs = strings.TrimSpace(posArgs)

	timeCalled   := bot.ReminderSys.parser.ArgCalledP("--time")
	inCalled     := bot.ReminderSys.parser.ArgCalledP("--in")
	listCalled   := bot.ReminderSys.parser.ArgCalledP("--list")
	deleteCalled := bot.ReminderSys.parser.ArgCalledP("--delete")

	if len(rawArgs) == 1 || (!timeCalled && !inCalled && !listCalled && !deleteCalled) {
		bot.ReminderSys.cmdRemindHelp(msg.ChannelID)
		return
	}

	if timeCalled && inCalled {
		bot.sendMsg(msg.ChannelID, "Sorry, both --time and --in cannot be used.  Please use one or the other.")
		return
	}

	if deleteCalled {
		remId := bot.ReminderSys.parser.GetArgSure("--delete").GetInt()

		if remId < 0 {
			bot.sendMsg(msg.ChannelID, "--delete expects a positive number.")
			return
		}

		if err := bot.ReminderSys.deleteReminder(remId, msg.Author.ID); err != nil {
			bot.sendMsg(msg.ChannelID, err.Error())
		} else {
			bot.sendMsg(msg.ChannelID, "Successfully deleted the reminder with ID %d", remId)
		}

		// Don't send another message too horrily fast...
		time.Sleep(142 * time.Millisecond)
	}

	if timeCalled || inCalled {
		var when time.Time
		var err error

		if timeCalled {
			when, err = getReminderTimeStr(bot.ReminderSys.parser.GetArgSure("--time").GetString())

			if err != nil {
				bot.sendMsg(msg.ChannelID, "Sorry, invalid RFC 3339 time string passed to --time")
				return
			}
		} else if inCalled {
			when, err = getReminderTimeOffset(bot.ReminderSys.parser.GetArgSure("--in").GetString())

			if err != nil {
				bot.sendMsg(msg.ChannelID, err.Error())
				return
			}
		}

		id, err := strconv.ParseInt(msg.Author.ID, 10, 64)
		if err != nil {
			logError("Could not get numeric user ID for %q\n", msg.Author.ID)
			bot.sendMainAdminDM("Could not get numeric user ID for %q\n", msg.Author.ID)
			bot.sendMsg(msg.ChannelID, "Uh oh!  Internall error.  The admin has been notified.")
			return
		}

		if posArgs == "" {
			bot.sendMsg(msg.ChannelID, "Whoops, looks like you didn't give me a message to remind you about.")
			return
		}

		bot.ReminderSys.In <- &Reminder{
			-1,
			id,
			when,
			posArgs}

		bot.sendMsg(msg.ChannelID, "Ok!  You'll be notified in a direct message on approximately %s",
			when.Format(time.UnixDate))

		if listCalled{
			// Don't send another message too horrily fast...
			time.Sleep(142 * time.Millisecond)
		}
	}

	if listCalled {
		rems, err := bot.ReminderSys.getUserReminders(msg.Author.ID)

		if err != nil {
			bot.sendMainAdminDM("Couldn't retrieve reminder list for user %s: %s", msg.Author.ID, err)
			bot.sendMsg(msg.ChannelID, "Error retrieving your reminder list.  The admin has been notified.")
		} else {
			var build strings.Builder

			build.WriteString("**Your reminders:**\n\n")
			for _, rem := range(rems) {
				build.WriteString(fmt.Sprintf("**ID %d** (%s):\n", rem.Id, rem.When.Format(time.UnixDate)))

				if len(rem.Message) > RemindersMaxListLen {
					build.WriteString(rem.Message[:(RemindersMaxListLen - 2)])
					build.WriteString("...")
				} else {
					build.WriteString(rem.Message)
				}

				build.WriteString("\n\n")
			}

			bot.sendMsg(msg.ChannelID, "%s", build.String())
		}
	}
}
