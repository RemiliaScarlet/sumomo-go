local sumomo = require("sumomo")

function sumomoDescription()
   return "Converts Centimeters to Inches."
end

function sumomoPerms()
   return "none"
end

function sumomoCommand(server, channel, author, args)
   local numArgs = sumomo.TableLen(args)

   if numArgs == 2 then
      if args[2] == "--help" or args[2] == "-h" then
         return "Usage: **!cmtoin** _<number>_\n"
      end
   end

   cm = tonumber(args[2])
   if cm == nil then
      return "I can't understand that number ┐(￣ヘ￣;)┌"
   end

   inches = cm / 2.54

   ret = string.format("%.2f° cm = %.2f° inches", cm, inches)

   if numArgs > 2 then
      ret = ret .. "\n\nbtw... I can only look at one number at a time (^_~)"
   end

   return ret
end
