// Sumomo-Go
// Copyright(C) 2018-2019 Alexa Jones-Gonzales
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/.>
package main

// //
// // !dict command
// //

// import (
// 	"encoding/json"
// 	"fmt"
// 	"net/http"
// 	"net/url"
// 	"strings"

// 	"github.com/bwmarrin/discordgo"
// )

// type DictSubsystem struct {
// 	bot *SumomoInstance
// 	cache map[string]string
// }

// func NewDictSubsystem() *DictSubsystem {
// 	return &DictSubsystem{
// 		nil,
// 		make(map[string]string, 0),
// 	}
// }

// func (sys *DictSubsystem) Init(bot *SumomoInstance) error {
// 	sys.bot = bot
// 	return nil
// }

// //
// // Holds a single definition
// //
// type Definition struct {
// 	Def string `json:"definition"`
// 	Example string `json:"example"`
// 	Synonyms []string `json:"synonyms"`
// }

// //
// // Holds a list of definitions for a single word
// //
// type DefinitionList struct {
// 	Word string `json:"word"`
// 	Phonetics []string `json:"phonetic"`
// 	Pronunciation string `json:"pronunciation"`
// 	Meanings map[string][]Definition `json:"meaning"`
// }

// //
// // Given a word, lookup its definition.  On success, a *DefinitionList
// // and nil are returned.  Otherwise nil and an error are returned.
// //
// func (sys *DictSubsystem) lookupWordDefinition(word string) (*DefinitionList, error) {
// 	logInfo("Looking up definition for: %q\n", word)
// 	params := url.Values{}
// 	params.Add("define", word)

// 	logDebug("Word definition lookup url: %q\n", DictUrl + params.Encode())
// 	response, err := http.Get(DictUrl + params.Encode())
// 	defer response.Body.Close()

// 	if err != nil {
// 		logDebug("HTTP response error for word definition lookup\n")
// 		return nil, err
// 	}

// 	var ret DefinitionList
// 	if err = json.NewDecoder(response.Body).Decode(&ret); err != nil {
// 		logDebug("JSON decoding error for word definition lookup\n")
// 		return nil, err
// 	}

// 	return &ret, nil
// }

// //
// // Given a word, lookup its definitions and build a string containing
// // the definitions.  On success, this returns the string and nil.  On
// // error, this returns an empty string and the error.
// //
// func (sys *DictSubsystem) buildDictResponse(word string) (string, error) {
// 	def, err := sys.lookupWordDefinition(word)
// 	if err != nil {
// 		return "", err
// 	}

// 	logDebug("Building definitions for %q\n", word)
// 	var ret strings.Builder
// 	ret.WriteString(def.Word)

// 	// Add phonetic pronunciation info
// 	if len(def.Phonetics) > 0 {
// 		ret.WriteString(" (")

// 		for idx, phon := range(def.Phonetics) {
// 			ret.WriteString(phon)

// 			if idx != len(def.Phonetics) - 1 {
// 				ret.WriteString(", ")
// 			}
// 		}

// 		ret.WriteString(")\n")
// 	}

// 	if def.Pronunciation != "" {
// 		ret.WriteString(fmt.Sprintf("<%s>", def.Pronunciation))
// 	}

// 	// Add the actual definitions
// 	for posType, pos := range(def.Meanings) {
// 		for _, meaning := range(pos) {
// 			ret.WriteString(fmt.Sprintf("**%s:** %s\n", posType, meaning.Def))

// 			if meaning.Example != "" {
// 				ret.WriteString(fmt.Sprintf("_example:_ %s\n", meaning.Example))
// 			}

// 			// Also add any synonyms.  This is per-definition.
// 			if len(meaning.Synonyms) > 0 {
// 				ret.WriteString("_synonyms:_ ")

// 				for idx, syn := range(meaning.Synonyms) {
// 					ret.WriteString(syn)

// 					if idx != len(meaning.Synonyms) - 1 {
// 						ret.WriteString(", ")
// 					}
// 				}
// 			}

// 			ret.WriteString("\n\n")
// 		}
// 	}

// 	retStr := ret.String()
// 	logDebug("Definitions built: %q\n", retStr)
// 	return retStr, nil
// }

// //
// // Implements the !define command
// //
// func (bot *SumomoInstance) cmdDefine(msg *discordgo.MessageCreate, args []string) {
// 	logDebug("cmdDefine called: %q\n", args)

// 	if len(args) == 1 {
// 		bot.sendMsg(msg.ChannelID, "No word given?")
// 	} else if len(args) > 2 {
// 		bot.sendMsg(msg.ChannelID, "One word at a time please")
// 	} else {
// 		bot.Instance.ChannelTyping(msg.ChannelID)

// 		if cached, found := bot.DictSys.cache[strings.ToLower(args[1])]; found {
// 			logDebug("Found cached definition for %s\n", strings.ToLower(args[1]))
// 			bot.sendMsg(msg.ChannelID, "%s", cached)
// 			return
// 		} else {
// 			logDebug("No cached definition for %s\n", strings.ToLower(args[1]))
// 		}

// 		if ret, err := bot.DictSys.buildDictResponse(args[1]); err != nil {
// 			// *sigh*
// 			bot.sendMsg(msg.ChannelID, "Couldn't find a definition.  Sometimes it might take a few attempts.")
// 		} else {
// 			logDebug("Caching definition for %s\n", strings.ToLower(args[1]))
// 			bot.DictSys.cache[strings.ToLower(args[1])] = ret
// 			bot.sendMsg(msg.ChannelID, "%s", ret)
// 		}
// 	}
// }
